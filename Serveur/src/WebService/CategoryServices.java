package WebService;

import Controller.MainController;
import Model.Category;
import Utils.TokenManagement;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * This class implements all services of the rest api related to categories management
 * @author mbervet
 */
@Path("/Category")
public class CategoryServices {

    /**
     * A reference to the main controller
     */
    public MainController MainController = Controller.MainController.getInstance();

    /**
     * The creation service
     * @param headers : the header of the http request normally containing an available token
     * @param name : the name of the new category
     * @param description : the description of the new category
     * @return String response : the service response to the request in json format
     */
    @POST
    @Path("/AddCategory")
    @Produces(MediaType.APPLICATION_JSON)
    public String addCategory(@Context HttpHeaders headers,
                              @FormParam("name") String name,
                              @FormParam("description") String description){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try{
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (name == null || description == null){
                throw new Exception("A given parameter is null.");
            }
            if (name.equals("") || description.equals("")){
                throw new Exception("All information has to be given.");
            }

            if (MainController.getCategoryController().addCategory(userEmail, name, description)){
                response.put("message", "New category registered.");
                response.put("registered", true);
            }
            else {
                response.put("message", "Registration failed");
                response.put("registered", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("registered", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the category list
     * @param headers : the header of the http request normally containing an available token
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/CategoryList")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategoryList(@Context HttpHeaders headers){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<Category> categoryList = MainController.getCategoryController().getCategoryList(userEmail);

            for (Category category : categoryList){
                JSONObject categoryInfo = new JSONObject();

                categoryInfo.put("description", category.getDescription());

                response.put(category.getName(), categoryInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to category information
     * @param headers : the header of the http request normally containing an available token
     * @param name : the category name to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/CategoryInfo/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategoryInformation(@Context HttpHeaders headers, @PathParam("name") String name){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            Category category = MainController.getCategoryController().getCategoryInformation(userEmail, name);

            JSONObject categoryInfo = new JSONObject();

            categoryInfo.put("description", category.getDescription());

            response.put(category.getName(), categoryInfo);

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The modification service
     * @param headers : the header of the http request normally containing an available token
     * @param currentName : the current name of the category
     * @param newName : the new name of the category
     * @param description : the new description of the category
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ModifyCategoryInfo")
    @Produces(MediaType.APPLICATION_JSON)
    public String modifyCategoryInformation(@Context HttpHeaders headers,
                                            @FormParam("currentName") String currentName,
                                            @FormParam("newName") String newName,
                                            @FormParam("description") String description){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getCategoryController().modifyCategoryInformation(userEmail, currentName, newName, description)){
                response.put("message", "Category information modified.");
                response.put("modified", true);
            }
            else {
                response.put("message", "Modification failed");
                response.put("modified", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("modified", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The deletion service
     * @param headers : the header of the http request normally containing an available token
     * @param name : the category name to look for
     * @return String response : the service response to the request in json format
     */
    @DELETE
    @Path("/DeleteCategory")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteCategory(@Context HttpHeaders headers, @FormParam("name") String name){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getCategoryController().deleteCategory(userEmail, name)){
                response.put("message", "Category deleted.");
                response.put("deleted", true);
            }
            else {
                response.put("message", "Deletion failed");
                response.put("deleted", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("deleted", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }
}
