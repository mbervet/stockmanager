package WebService;

import Controller.MainController;
import Model.Product;
import Utils.TokenManagement;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * This class implements all services of the rest api related to products management
 * @author mbervet
 */
@Path("/Product")
public class ProductServices {

    /**
     * A reference to the main controller
     */
    private MainController MainController = Controller.MainController.getInstance();

    /**
     * The creation service
     * @param headers : the header of the http request normally containing an available token
     * @param name : the name of the new category
     * @param description : the description of the new category
     * @param price : the price of the new product
     * @param categoryName : the category name of the new product
     * @return String response : the service response to the request in json format
     */
    @POST
    @Path("/AddProduct")
    @Produces(MediaType.APPLICATION_JSON)
    public String addProduct(@Context HttpHeaders headers,
                             @FormParam("name") String name,
                             @FormParam("description") String description,
                             @FormParam("price") String price,
                             @FormParam("categoryName") String categoryName){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try{
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (name == null || description == null || price == null || categoryName == null){
                throw new Exception("A given parameter is null.");
            }
            if (name.equals("") || description.equals("") || price.equals("") || categoryName.equals("")){
                throw new Exception("All information has to be given.");
            }

            if (MainController.getProductController().addProduct(userEmail, name, description, Double.parseDouble(price), categoryName)){
                response.put("message", "New product registered.");
                response.put("registered", true);
            }
            else {
                response.put("message", "Registration failed");
                response.put("registered", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("registered", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the product list
     * @param headers : the header of the http request normally containing an available token
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ProductList")
    @Produces(MediaType.APPLICATION_JSON)
    public String getProductList(@Context HttpHeaders headers){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<Product> productList = MainController.getProductController().getProductList(userEmail);

            for (Product product : productList){
                JSONObject productInfo = new JSONObject();

                productInfo.put("description", product.getDescription());

                response.put(product.getName(), productInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the product list in a category
     * @param headers : the header of the http request normally containing an available token
     * @param categoryName : the category name to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ProductByCategory/{categoryName}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getProductByCategory(@Context HttpHeaders headers, @PathParam("categoryName") String categoryName){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<Product> productList = MainController.getProductController().getProductListByCategory(userEmail, categoryName);

            for (Product product : productList){
                JSONObject productInfo = new JSONObject();

                productInfo.put("description", product.getDescription());

                response.put(product.getName(), productInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to product information
     * @param headers : the header of the http request normally containing an available token
     * @param name : the product name to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ProductInfo/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getProductInformation(@Context HttpHeaders headers, @PathParam("name") String name){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            Product product = MainController.getProductController().getProductInformationByName(userEmail, name);

            JSONObject productInfo = new JSONObject();

            productInfo.put("description", product.getDescription());

            response.put(product.getName(), productInfo);

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service allowing to change the category of a product
     * @param headers : the header of the http request normally containing an available token
     * @param name : the product name to look for
     * @param categoryName : the new  product category name
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ChangeCategory")
    @Produces(MediaType.APPLICATION_JSON)
    public String changeCategory(@Context HttpHeaders headers,
                                 @FormParam("name") String name,
                                 @FormParam("categoryName") String categoryName){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getProductController().changeCategory(userEmail, name, categoryName)){
                response.put("message", "Product category changed.");
                response.put("changed", true);
            }
            else {
                response.put("message", "Change failed");
                response.put("changed", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("changed", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The modification service
     * @param headers : the header of the http request normally containing an available token
     * @param currentName : the current name of the product
     * @param newName : the new name of the product
     * @param description : the new description of the product
     * @param price : the new price of the product
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ModifyProductInfo")
    @Produces(MediaType.APPLICATION_JSON)
    public String modifyProductInfo(@Context HttpHeaders headers,
                                    @FormParam("currentName") String currentName,
                                    @FormParam("newName") String newName,
                                    @FormParam("description") String description,
                                    @FormParam("price") String price){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getProductController().modifyProductInformation(userEmail, currentName, newName, description, Double.parseDouble(price))){
                response.put("message", "Product information modified.");
                response.put("modified", true);
            }
            else {
                response.put("message", "Modification failed");
                response.put("modified", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("modified", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The deletion service
     * @param headers : the header of the http request normally containing an available token
     * @param name : the product name to look for
     * @return String response : the service response to the request in json format
     */
    @DELETE
    @Path("/DeleteProduct")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteProduct(@Context HttpHeaders headers, @FormParam("name") String name){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getProductController().deleteProduct(userEmail, name)){
                response.put("message", "Product deleted.");
                response.put("deleted", true);
            }
            else {
                response.put("message", "Deletion failed");
                response.put("deleted", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("deleted", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }
}
