package WebService;

import Controller.MainController;
import Model.Shop;
import Utils.TokenManagement;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * This class implements all services of the rest api related to shops management
 * @author mbervet
 */
@Path("/Shop")
public class ShopServices {

    /**
     * A reference to the main controller
     */
    private MainController MainController = Controller.MainController.getInstance();

    /**
     * The shop creation service
     * @param headers : the header of the http request normally containing an available token
     * @param address : the address of the new shop
     * @param town : the town of the new shop
     * @param managerEmail : the manager email of the new shop
     * @return String response : the service response to the request in json format
     */
    @POST
    @Path("/AddShop")
    @Produces(MediaType.APPLICATION_JSON)
    public String addShop(@Context HttpHeaders headers,
                          @FormParam("address") String address,
                          @FormParam("town") String town,
                          @FormParam("managerEmail") String managerEmail){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try{
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (address == null || town == null || managerEmail == null){
                throw new Exception("A given parameter is null.");
            }
            if (address.equals("") || town.equals("") || managerEmail.equals("")){
                throw new Exception("All information has to be given.");
            }

            if (MainController.getShopController().addShop(userEmail, address, town, managerEmail)){
                response.put("message", "New shop registered.");
                response.put("registered", true);
            }
            else {
                response.put("message", "Registration failed");
                response.put("registered", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("registered", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the shop list
     * @param headers : the header of the http request normally containing an available token
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ShopList")
    @Produces(MediaType.APPLICATION_JSON)
    public String getShopList(@Context HttpHeaders headers){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<Shop> shopList = MainController.getShopController().getShopList(userEmail);

            for (Shop shop : shopList){
                JSONObject shopInfo = new JSONObject();

                shopInfo.put("address", shop.getAddress());
                shopInfo.put("town", shop.getTown());
                shopInfo.put("manager", shop.getManager().getEmail());

                response.put(shop.getId().toString(), shopInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to shop information
     * @param headers : the header of the http request normally containing an available token
     * @param shopId : the shop id to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ShopInfo/{shopId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getShopInformation(@Context HttpHeaders headers, @PathParam("shopId") String shopId){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            Shop shop = MainController.getShopController().getShopInformationById(userEmail, Integer.parseInt(shopId));

            JSONObject shopInfo = new JSONObject();

            shopInfo.put("address", shop.getAddress());
            shopInfo.put("town", shop.getTown());
            shopInfo.put("manager", shop.getManager().getEmail());

            response.put(shop.getId().toString(), shopInfo);

            response.put("found", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("found", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service allowing to change the manager of a shop
     * @param headers : the header of the http request normally containing an available token
     * @param shopId : the shop id to look for
     * @param managerEmail : the new shop manager email
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ChangeManager")
    @Produces(MediaType.APPLICATION_JSON)
    public String changeManager(@Context HttpHeaders headers,
                                @FormParam("shopId") String shopId,
                                @FormParam("managerEmail") String managerEmail){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getShopController().changeManager(userEmail, Integer.parseInt(shopId), managerEmail)){
                response.put("message", "Shop manager changed.");
                response.put("changed", true);
            }
            else {
                response.put("message", "Change failed");
                response.put("changed", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("changed", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service to modify shop information
     * @param headers : the header of the http request normally containing an available token
     * @param shopId : the shop id to look for
     * @param address : the new shop address
     * @param town : the new shop town
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ModifyShopInfo")
    @Produces(MediaType.APPLICATION_JSON)
    public String modifyShopInformation(@Context HttpHeaders headers,
                                        @FormParam("shopId") String shopId,
                                        @FormParam("address") String address,
                                        @FormParam("town") String town){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getShopController().modifyShopInformation(userEmail, Integer.parseInt(shopId), address, town)){
                response.put("message", "Shop information modified.");
                response.put("modified", true);
            }
            else {
                response.put("message", "Modification failed");
                response.put("modified", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("modified", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The deletion service
     * @param headers : the header of the http request normally containing an available token
     * @param shopId : the shop id to look for
     * @return String response : the service response to the request in json format
     */
    @DELETE
    @Path("/DeleteShop")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteShop(@Context HttpHeaders headers, @FormParam("shopId") String shopId){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getShopController().deleteShop(userEmail, Integer.parseInt(shopId))){
                response.put("message", "Shop deleted.");
                response.put("deleted", true);
            }
            else {
                response.put("message", "Deletion failed");
                response.put("deleted", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("deleted", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }
}
