package WebService;

import Controller.MainController;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import Model.User;
import Utils.TokenManagement;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * This class implements all services of the rest api related to users management
 * @author mbervet
 */
@Path("/User")
public class UserServices extends Application {

    /**
     * A reference to the main controller
     */
    private MainController MainController = Controller.MainController.getInstance();

    /**
     * The registering service
     * @param email : the user email to register
     * @param password : the user password to register
     * @param firstName : the user first name to register
     * @param lastName : the user last name to register
     * @return String response : the service response to the request in json format
     */
    @POST
    @Path("/Register")
    @Produces(MediaType.APPLICATION_JSON)
    public String register(@FormParam("email") String email,
                           @FormParam("password") String password,
                           @FormParam("firstName") String firstName,
                           @FormParam("lastName") String lastName){
        JSONObject response = new JSONObject();

        try{
            if (email == null || password == null || firstName == null || lastName == null){
                throw new Exception("A given parameter is null.");
            }
            if (email.equals("") || password.equals("") || firstName.equals("") || lastName.equals("")){
                throw new Exception("All information has to be given.");
            }

            if (MainController.getUserController().register(email, password, firstName, lastName)){
                response.put("message", firstName + " " + lastName + " registered.");
                response.put("registered", true);
            }
            else {
                response.put("message", "Registration failed");
                response.put("registered", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("registered", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * Connection service
     * @param email : the email to look for
     * @param password : the password to check
     * @return String response : the service response to the request in json format
     */
    @POST
    @Path("/SignIn")
    @Produces(MediaType.APPLICATION_JSON)
    public String signIn(@FormParam("email") String email, @FormParam("password") String password){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            if (MainController.getUserController().signIn(email, password)){
                tokenManagement = new TokenManagement();
                User user = MainController.getUserController().getUserInformationByEmail(email, email);
                response.put("token", tokenManagement.generateToken(email));
                response.put("message", "You are connected as " + user.getFirstName() + " " + user.getLastName());
                response.put("connected", true);
            }
            else {
                response.put("message", "Wrong email or password");
                response.put("connected", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("connected", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The disconnection service
     * @param headers : the header of the http request normally containing an available token
     * @return String response : the service response to the request in json format
     */
    @POST
    @Path("/SignOut")
    @Produces(MediaType.APPLICATION_JSON)
    public String signOut(@Context HttpHeaders headers){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getUserController().signOut(userEmail)){
                User user = MainController.getUserController().getUserInformationByEmail(userEmail, userEmail);
                response.put("message", user.getFirstName() + " " + user.getLastName() + " disconnected.");
                response.put("disconnected", true);
            }
            else {
                response.put("message", "Disconnection failed");
                response.put("disconnected", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("disconnected", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the user list
     * @param headers : the header of the http request normally containing an available token
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/UserList")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUserList(@Context HttpHeaders headers){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<User> userList = MainController.getUserController().getUserList(userEmail);

            for (User user : userList){
                JSONObject userInfo = new JSONObject();

                userInfo.put("firstName", user.getFirstName());
                userInfo.put("lastName", user.getLastName());
                userInfo.put("type", user.getType().toString());

                response.put(user.getEmail(), userInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to user information
     * @param headers : the header of the http request normally containing an available token
     * @param email : the email to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/UserInfo/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUserInformation(@Context HttpHeaders headers, @PathParam("email") String email){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            User user = MainController.getUserController().getUserInformationByEmail(userEmail, email);

            if (user != null){
                JSONObject userInfo = new JSONObject();

                userInfo.put("firstName", user.getFirstName());
                userInfo.put("lastName", user.getLastName());
                userInfo.put("type", user.getType().toString());

                response.put(user.getEmail(), userInfo);
                response.put("found", true);
            }
            else {
                response.put("message", email + " doesn't refer to an existing user.");
                response.put("found", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("found", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service to modify user information
     * @param headers : the header of the http request normally containing an available token
     * @param currentEmail : the current email of the user
     * @param newEmail : the new email of the user
     * @param password : the new password of the user
     * @param firstName : the new first name of the user
     * @param lastName : the new last name of the user
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ModifyUserInfo")
    @Produces(MediaType.APPLICATION_JSON)
    public String modifyUserInformation(@Context HttpHeaders headers,
                                        @FormParam("currentEmail") String currentEmail,
                                        @FormParam("newEmail") String newEmail,
                                        @FormParam("password") String password,
                                        @FormParam("firstName") String firstName,
                                        @FormParam("lastName") String lastName) {
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getUserController().modifyUserInformation(userEmail, currentEmail, newEmail, password, firstName, lastName)){
                response.put("message", "User information modified.");
                response.put("modified", true);
            }
            else {
                response.put("message", "Modification failed");
                response.put("modified", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("modified", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The promotion service
     * @param headers : the header of the http request normally containing an available token
     * @param email : the email of the user to promote
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/PromoteUser")
    @Produces(MediaType.APPLICATION_JSON)
    public String promoteUser(@Context HttpHeaders headers, @FormParam("email") String email){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getUserController().promote(userEmail, email)){
                User user = MainController.getUserController().getUserInformationByEmail(userEmail, email);
                response.put("message", user.getFirstName() + " " + user.getLastName() + " promoted.");
                response.put("promoted", true);
            }
            else {
                response.put("message", "Promotion failed");
                response.put("promoted", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getStackTrace());
                response.put("promoted", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service allowing to revoke all rights of a user
     * @param headers : the header of the http request normally containing an available token
     * @param email : the email of the user to revoke rights
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/RevokeUserRights")
    @Produces(MediaType.APPLICATION_JSON)
    public String revokeUserRights(@Context HttpHeaders headers, @FormParam("email") String email){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getUserController().revokeRights(userEmail, email)){
                User user = MainController.getUserController().getUserInformationByEmail(userEmail, email);
                response.put("message", user.getFirstName() + " " + user.getLastName() + " rights revoked.");
                response.put("revoked", true);
            }
            else {
                response.put("message", "Revoking failed");
                response.put("revoked", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("revoked", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The firing service
     * @param headers : the header of the http request normally containing an available token
     * @param email : the email of the user to fire
     * @return String response : the service response to the request in json format
     */
    @DELETE
    @Path("/FiredUser")
    @Produces(MediaType.APPLICATION_JSON)
    public String firedUser(@Context HttpHeaders headers, @FormParam("email") String email){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getUserController().firedUser(userEmail, email)){
                response.put("message", email + " fired.");
                response.put("fired", true);
            }
            else {
                response.put("message", "Firing failed");
                response.put("fired", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("fired", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }
}
