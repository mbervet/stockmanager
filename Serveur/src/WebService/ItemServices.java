package WebService;

import Controller.MainController;
import Model.Item;
import Utils.TokenManagement;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * This class implements all services of the rest api related to items management
 * @author mbervet
 */
@Path("/Item")
public class ItemServices {

    /**
     * A reference to the main controller
     */
    private MainController MainController = Controller.MainController.getInstance();

    /**
     * The creation service
     * @param headers : the header of the http request normally containing an available token
     * @param code : the code of the new item
     * @param shelfId : the shelf id of the new item
     * @param productName : the product name of the new item
     * @return String response : the service response to the request in json format
     */
    @POST
    @Path("/AddItem")
    @Produces(MediaType.APPLICATION_JSON)
    public String addItem(@Context HttpHeaders headers,
                          @FormParam("code") String code,
                          @FormParam("shelfId") String shelfId,
                          @FormParam("productName") String productName){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try{
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (code == null || shelfId == null || productName == null){
                throw new Exception("A given parameter is null.");
            }
            if (code.equals("") || shelfId.equals("") || productName.equals("")){
                throw new Exception("All information has to be given.");
            }

            if (MainController.getItemController().addItem(userEmail, code, Integer.parseInt(shelfId), productName)){
                response.put("message", "New item registered.");
                response.put("registered", true);
            }
            else {
                response.put("message", "Registration failed");
                response.put("registered", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("registered", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the item list
     * @param headers : the header of the http request normally containing an available token
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ItemList")
    @Produces(MediaType.APPLICATION_JSON)
    public String getItemList(@Context HttpHeaders headers){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<Item> itemList = MainController.getItemController().getItemList(userEmail);

            for (Item item : itemList){
                JSONObject itemInfo = new JSONObject();

                itemInfo.put("product", item.getProduct().getName());
                itemInfo.put("shelf", item.getShelf().getId());

                response.put(item.getCode(), itemInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the item list in a product
     * @param headers : the header of the http request normally containing an available token
     * @param productName : the product name to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ItemByProduct/{productName}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getItemByProduct(@Context HttpHeaders headers, @PathParam("productName") String productName){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<Item> itemList = MainController.getItemController().getItemListByProduct(userEmail, productName);

            for (Item item : itemList){
                JSONObject itemInfo = new JSONObject();

                itemInfo.put("product", item.getProduct().getName());
                itemInfo.put("shelf", item.getShelf().getId());

                response.put(item.getCode(), itemInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to item information
     * @param headers : the header of the http request normally containing an available token
     * @param code : the item code to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ItemInfo/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getItemInformation(@Context HttpHeaders headers, @PathParam("code") String code){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            Item item = MainController.getItemController().getItemInformationByCode(userEmail, code);

            JSONObject itemInfo = new JSONObject();

            itemInfo.put("product", item.getProduct().getName());
            itemInfo.put("shelf", item.getShelf().getId());

            response.put(item.getCode(), itemInfo);

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service allowing to change the product of an item
     * @param headers : the header of the http request normally containing an available token
     * @param code : the item code to look for
     * @param productName : the new  item product name
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ChangeProduct")
    @Produces(MediaType.APPLICATION_JSON)
    public String changeProduct(@Context HttpHeaders headers,
                                @FormParam("code") String code,
                                @FormParam("productName") String productName){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getItemController().changeProduct(userEmail, code, productName)){
                response.put("message", "Item product changed.");
                response.put("changed", true);
            }
            else {
                response.put("message", "Change failed");
                response.put("changed", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("changed", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service allowing to change the shelf of an item
     * @param headers : the header of the http request normally containing an available token
     * @param code : the item code to look for
     * @param shelfId : the new  item shelf id
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ChangeShelf")
    @Produces(MediaType.APPLICATION_JSON)
    public String changeShelf(@Context HttpHeaders headers,
                              @FormParam("code") String code,
                              @FormParam("shelfId") String shelfId){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getItemController().changeShelf(userEmail, code, Integer.parseInt(shelfId))){
                response.put("message", "Item shelf changed.");
                response.put("changed", true);
            }
            else {
                response.put("message", "Change failed");
                response.put("changed", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("changed", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The deletion service
     * @param headers : the header of the http request normally containing an available token
     * @param code : the item code to look for
     * @return String response : the service response to the request in json format
     */
    @DELETE
    @Path("/DeleteItem")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteItem(@Context HttpHeaders headers, @FormParam("code") String code){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getItemController().deleteItem(userEmail, code)){
                response.put("message", "Item deleted.");
                response.put("deleted", true);
            }
            else {
                response.put("message", "Deletion failed");
                response.put("deleted", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("deleted", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }
}
