package WebService;

import Controller.MainController;
import Model.Shelf;
import Utils.TokenManagement;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * This class implements all services of the rest api related to shelf management
 * @author mbervet
 */
@Path("/Shelf")
public class ShelfServices {

    /**
     * A reference to the main controller
     */
    private MainController MainController = Controller.MainController.getInstance();

    /**
     * The shop creation service
     * @param headers : the header of the http request normally containing an available token
     * @param managerEmail : the shelf manager email
     * @param shopId : the shelf shop id
     * @return String response : the service response to the request in json format
     */
    @POST
    @Path("/AddShelf")
    @Produces(MediaType.APPLICATION_JSON)
    public String addShelf(@Context HttpHeaders headers,
                           @FormParam("managerEmail") String managerEmail,
                           @FormParam("shopId") String shopId){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try{
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (managerEmail == null || shopId == null){
                throw new Exception("A given parameter is null.");
            }
            if (managerEmail.equals("") || shopId.equals("")){
                throw new Exception("All information has to be given.");
            }

            if (MainController.getShelfController().addShelf(userEmail, managerEmail, Integer.parseInt(shopId))){
                response.put("message", "New shelf registered.");
                response.put("registered", true);
            }
            else {
                response.put("message", "Registration failed");
                response.put("registered", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("registered", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the shelf list
     * @param headers : the header of the http request normally containing an available token
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ShelfList")
    @Produces(MediaType.APPLICATION_JSON)
    public String getShelfList(@Context HttpHeaders headers){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<Shelf> shelfList = MainController.getShelfController().getShelfList(userEmail);

            for (Shelf shelf : shelfList){
                JSONObject shelfInfo = new JSONObject();

                shelfInfo.put("manager", shelf.getManager().getEmail());
                shelfInfo.put("shop", shelf.getShop().getId());

                response.put(shelf.getId().toString(), shelfInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to the shelf list of a shop
     * @param headers : the header of the http request normally containing an available token
     * @param shopId : the shop id to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ShelfByShop/{shopId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getShelfByShop(@Context HttpHeaders headers, @PathParam("shopId") String shopId){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            List<Shelf> shelfList = MainController.getShelfController().getShelfListByShop(userEmail, Integer.parseInt(shopId));

            for (Shelf shelf : shelfList){
                JSONObject shelfInfo = new JSONObject();

                shelfInfo.put("manager", shelf.getManager().getEmail());
                shelfInfo.put("shop", shelf.getShop().getId());

                response.put(shelf.getId().toString(), shelfInfo);
            }

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The access service to shelf information
     * @param headers : the header of the http request normally containing an available token
     * @param shelfId : the shelf id to look for
     * @return String response : the service response to the request in json format
     */
    @GET
    @Path("/ShelfInfo/{shelfId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getShelfInformation(@Context HttpHeaders headers, @PathParam("shelfId") String shelfId){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            Shelf shelf = MainController.getShelfController().getShelfInformationById(userEmail, Integer.parseInt(shelfId));

            JSONObject shelfInfo = new JSONObject();

            shelfInfo.put("manager", shelf.getManager().getEmail());
            shelfInfo.put("shop", shelf.getShop().getId());

            response.put(shelf.getId().toString(), shelfInfo);

            response.put("returned", true);
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("returned", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service allowing to change the manager of a shelf
     * @param headers : the header of the http request normally containing an available token
     * @param shelfId : the shelf id to look for
     * @param managerEmail : the new shelf manager email
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ChangeManager")
    @Produces(MediaType.APPLICATION_JSON)
    public String changeManager(@Context HttpHeaders headers,
                                @FormParam("shelfId") String shelfId,
                                @FormParam("managerEmail") String managerEmail){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getShelfController().changeManager(userEmail, Integer.parseInt(shelfId), managerEmail)){
                response.put("message", "Shelf manager changed.");
                response.put("changed", true);
            }
            else {
                response.put("message", "Change failed");
                response.put("changed", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("changed", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The service allowing to change the shop of a shelf
     * @param headers : the header of the http request normally containing an available token
     * @param shelfId : the shelf id to look for
     * @param shopId : the new shelf shop id
     * @return String response : the service response to the request in json format
     */
    @PUT
    @Path("/ChangeShop")
    @Produces(MediaType.APPLICATION_JSON)
    public String changeShop(@Context HttpHeaders headers,
                             @FormParam("shelfId") String shelfId,
                             @FormParam("shopId") String shopId){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getShelfController().changeShop(userEmail, Integer.parseInt(shelfId), Integer.parseInt(shopId))){
                response.put("message", "Shelf shop changed.");
                response.put("changed", true);
            }
            else {
                response.put("message", "Change failed");
                response.put("changed", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("changed", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }

    /**
     * The deletion service
     * @param headers : the header of the http request normally containing an available token
     * @param shelfId : the shelf id to look for
     * @return String response : the service response to the request in json format
     */
    @DELETE
    @Path("/DeleteShelf")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteShelf(@Context HttpHeaders headers, @FormParam("shelfId") String shelfId){
        TokenManagement tokenManagement;
        JSONObject response = new JSONObject();

        try {
            tokenManagement = new TokenManagement();
            String userEmail = tokenManagement.verifyToken(headers.getHeaderString("token"));

            if (userEmail == null){
                throw new Exception("Invalid token.");
            }

            if (MainController.getShelfController().deleteShelf(userEmail, Integer.parseInt(shelfId))){
                response.put("message", "Shelf deleted.");
                response.put("deleted", true);
            }
            else {
                response.put("message", "Deletion failed");
                response.put("deleted", false);
            }
        }
        catch (Exception e){
            try {
                response.put("message", e.getMessage());
                response.put("deleted", false);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        return response.toString();
    }
}
