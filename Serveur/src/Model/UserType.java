package Model;

public enum UserType {
    REGISTERED , SHELF_MANAGER, SHOP_MANAGER, CEO, ADMIN
}
