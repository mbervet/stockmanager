package Test.DAO;

import DAO.ShelfDAO;
import DAO.ShopDAO;
import Model.*;
import Test.Mock.ShopDAOMock;
import Test.Mock.UserDAOMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ShelfDAOTest {
    ShelfDAO shelfDAO;

    @BeforeEach
    void setUp() {
        UserDAOMock userDAOMock = new UserDAOMock();
        ShopDAOMock shopDAOMock = new ShopDAOMock();
        shelfDAO = new ShelfDAO(shopDAOMock, userDAOMock);

    }

    @Test
    void getShelfs() {
        List<Shelf> shelfList = shelfDAO.getShelfs();
        assertNotNull(shelfList);
        assertFalse(shelfList.isEmpty());
    }

    @Test
    void getShelfById() {
        Shelf shelf = shelfDAO.getShelfById(1);
        assertNotNull(shelf);
        assertEquals(1, shelf.getId());

    }

    @Test
    void getNotExistingShelfById() {
        Shelf shelf = shelfDAO.getShelfById(-1);
        assertNull(shelf);
    }

    @Test
    void addShelf() {
        int idShelf = -1;

        User user02 = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);
        Shop shop03 = new Shop( "address 03",  "town 02", user02);
        shop03.setId(3);
        try{
            idShelf = shelfDAO.addShelf(user02, shop03);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, idShelf);

    }

    @Test
    void addShelfShopNull() {

        User user02 = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);

            assertThrows(Exception.class, () ->{shelfDAO.addShelf(user02, null);},"The shelf shop has to be set.");

    }

    @Test
    void addShelfUserHaventRight() {
        User user02 = new User("email02@test.fr", "password", "firstName", "lastName", UserType.REGISTERED);
        Shop shop03 = new Shop( "address 03",  "town 02", user02);
        shop03.setId(3);

        assertThrows(Exception.class, () ->{shelfDAO.addShelf(user02, shop03);},"The user haven't the right to manage the shelf");
    }

    @Test
    void updateShelf() {
        int idShelf = -1;

        User user02 = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);
        Shop shop03 = new Shop( "address 03",  "town 02", user02);
        shop03.setId(3);
        try{
            idShelf = shelfDAO.updateShelf(2, user02, shop03);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, idShelf);
        assertEquals(2, idShelf);
    }

    @Test
    void updateShelfNotExisting() {
        User user02 = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);
        Shop shop03 = new Shop( "address 03",  "town 02", user02);
        shop03.setId(3);

        assertThrows(Exception.class, () ->{shelfDAO.updateShelf(-1, user02, shop03);},"This id doesn't refer to an existing shelf.");
    }

    @Test
    void updateShelfShopIsNull() {
        User user02 = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);

        assertThrows(Exception.class, () ->{shelfDAO.updateShelf(-1, user02, null);},"This id doesn't refer to an existing shelf.");
    }

    @Test
    void updateShelfShopNotExisting() {
        User user02 = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);
        Shop shop03 = new Shop( "address 03",  "town 02", user02);
        shop03.setId(-1);

        assertThrows(Exception.class, () ->{shelfDAO.updateShelf(-1, user02, shop03);},"This shop doesn't refer to an existing shop.");
    }

    @Test
    void updateShelfUserHaventRight() {
        User user02 = new User("email02@test.fr", "password", "firstName", "lastName", UserType.REGISTERED);
        Shop shop03 = new Shop( "address 03",  "town 02", user02);
        shop03.setId(3);

        assertThrows(Exception.class, () ->{shelfDAO.updateShelf(3,user02, shop03);},"The user haven't the right to manage the shelf");
    }


    @Test
    void deleteShelf() {
        boolean b = false;

        try{
            b = shelfDAO.deleteShelf(4);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertTrue(b);
    }
    @Test
    void deleteShelfNotEmpty() {

        assertThrows(Exception.class, () ->{shelfDAO.deleteShelf(-1);},"This id doesn't refer to an existing shelf.");
    }
    @Test
    void deleteShelfNotexisting() {

        assertThrows(Exception.class, () ->{shelfDAO.deleteShelf(5);},"This shelf still has items.");
    }
}