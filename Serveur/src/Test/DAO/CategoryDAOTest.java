package Test.DAO;

import DAO.CategoryDAO;
import Model.Category;
import Utils.HibernateUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoryDAOTest {
    CategoryDAO categoryDAO;

    @BeforeEach
    void setUp() {
        categoryDAO = new CategoryDAO();

    }

    @AfterEach
    void tearDown() {

    }

    /**
     * Test si la nous obtenons la catégorie par le nom avec une categorie existante
     */
    @Test
    void getExistingCategoryByName() {
        Category category = categoryDAO.getCategoryByName("category test 01");
        assertNotNull(category);
        assertEquals("category test 01", category.getName());
    }

    /**
     * Test si la nous n'obtenons pas la catégorie par le nom avec une categorie inexistante
     */
    @Test
    void getNotExistingCategoryByName() {
        Category category = categoryDAO.getCategoryByName("non categorie test 01");

        assertNull(category);
    }

    /**
     * Test si nous obtenons bien une liste de catégorie
     */
    @Test
    void getCategories() {
        List<Category> category = categoryDAO.getCategories();

        assertNotNull(category);
        assertFalse(category.isEmpty());
    }


    /**
     * Test de l'ajout d'une catégorie
     */
    @Test
    void addCategory() {
        int idCategory = -1;
        try{

           idCategory = categoryDAO.addCategory("test ajout", "test");
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, idCategory);
    }

    /**
     * Test de l'ajout d'un catégorie possedant un nom déjà existant
     */
    @Test
    void addExistingCategory() {
        assertThrows(Exception.class, () -> {categoryDAO.addCategory("category test 01", "test");}, "This category name already exist.");

    }

    /**
     * Test la mise à jour d'une catégorie existante
     */
    @Test
    void updateCategory() {
        int idCategory = -1;

        try{

            idCategory = categoryDAO.updateCategory("category to update", "category update", "desc update");
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, idCategory);
    }


    /**
     * Test de la mise à jour d'une catégorie existante
     */
    @Test
    void updateNotExistingCategory() {

        assertThrows(Exception.class, () -> {categoryDAO.updateCategory("category to not update",
                "category test 01", "desc update");}, "This name doesn't refer to an existing category.");
    }

    /**
     * Test de suppression de categorie existant
     */
    @Test
    void deleteCategoryEmptyProduct() {
        boolean deletedCategory = false;

        try{

            deletedCategory = categoryDAO.deleteCategory("category to delete no product");
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertTrue(deletedCategory);
    }

    /**
     * Test de suppression de categorie existant
     */
    @Test
    void deleteCategoryExistantProduct() {
        assertThrows(Exception.class, () -> {categoryDAO.deleteCategory("category to delete with product");},
                "This category still has products.");
    }

    /**
     * Test de suppression de categorie existant
     */
    @Test
    void deleteNotExistingCategory() {
        assertThrows(Exception.class, () -> {categoryDAO.deleteCategory("category to delete no product --");},
                "This name doesn't refer to an existing category.");
    }
}