package Test.DAO;

import DAO.CategoryDAO;
import DAO.ItemDAO;
import DAO.ProductDAO;
import Model.*;
import Test.Mock.CategoryDAOMock;
import Test.Mock.ProductDAOMock;
import Test.Mock.ShelfDAOMock;
import Test.Mock.UserDAOMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ItemDAOTest {

    ItemDAO itemDAO;
    ShelfDAOMock shelfDAOMock;
    ProductDAOMock productDAOMock;

    @BeforeEach
    void setUp() {
        productDAOMock = new ProductDAOMock();
        shelfDAOMock = new ShelfDAOMock();
        itemDAO = new ItemDAO(productDAOMock, shelfDAOMock);

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getItems() {
        List<Item> items = itemDAO.getItems();
        assertNotNull(items);
        assertFalse(items.isEmpty());

    }



    @Test
    void getItemByCode() {
        Item item = itemDAO.getItemByCode("item 01");
        assertNotNull(item);
        assertEquals("item 01", item.getCode());
    }

    @Test
    void getNotExistingItemByCode() {
        Item item = itemDAO.getItemByCode("item -01");
        assertNotNull(item);

    }

    @Test
    void addItem() {
        int itemId = -1;
        Shelf shelf = shelfDAOMock.getShelfById(1);
        Product product = productDAOMock.getProductByName("name 01");
        try{
            itemId = itemDAO.addItem("item add", shelf, product);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, itemId);
    }

    @Test
    void addItemExistingCode() {
        Shelf shelf = shelfDAOMock.getShelfById(1);
        Product product = productDAOMock.getProductByName("name 01");
        assertThrows(Exception.class, () ->{itemDAO.addItem("item 01", shelf, product);},"This item code already exist.");
    }

    @Test
    void addItemProductNull() {
        Shelf shelf = shelfDAOMock.getShelfById(1);
        assertThrows(Exception.class, () ->{itemDAO.addItem("item 01", shelf, null);},"The item product has to be set.");
    }



    @Test
    void updateItem() {
        int itemId = -1;
        Shelf shelf = shelfDAOMock.getShelfById(1);
        Product product = productDAOMock.getProductByName("name 02");
        try{
            itemId = itemDAO.updateItem("item update", shelf, product);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, itemId);
    }
    @Test
    void updateNotExistingItem() {

        Shelf shelf = shelfDAOMock.getShelfById(1);
        Product product = productDAOMock.getProductByName("name 01");
        assertThrows(Exception.class, () ->{itemDAO.updateItem("item -update", shelf, product);},"The item product has to be set.");
    }

    @Test
    void updateItemProductNull() {

        Shelf shelf = shelfDAOMock.getShelfById(1);
        assertThrows(Exception.class, () ->{itemDAO.updateItem("item update", shelf, null);},"The item product has to be set.");
    }



    @Test
    void deleteItem() {
        boolean b = false;
        Shelf shelf = shelfDAOMock.getShelfById(1);
        Product product = productDAOMock.getProductByName("name 02");
        try{
                b = itemDAO.deleteItem("item delete");
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertTrue(b);
    }

    @Test
    void deleteNotExistingItem() {
        assertThrows(Exception.class, () ->{itemDAO.deleteItem("item -delete");},"The item product has to be set.");
    }
}