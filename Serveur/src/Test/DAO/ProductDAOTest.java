package Test.DAO;

import DAO.ProductDAO;
import Model.Category;
import Model.Product;
import Test.Mock.CategoryDAOMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductDAOTest {
    ProductDAO productDAO;
    @BeforeEach
    void setUp() {
        productDAO = new ProductDAO(new CategoryDAOMock());

    }

    @Test
    void getProducts() {
        List<Product> productList = productDAO.getProducts();
        assertNotNull(productList);
        assertFalse(productList.isEmpty());

    }

    @Test
    void getProductByName() {
        Product product = productDAO.getProductByName("product 01");
        assertNotNull(product);
        assertEquals("product 01", product.getName());


    }

    @Test
    void getNotExistingProductByName() {
        Product product = productDAO.getProductByName("product -01");
        assertNull(product);


    }

    @Test
    void addProduct() {
        int idProduct = -1;
        Category cat01 = new Category("categorie 1", "descr 1");
        try{
            idProduct = productDAO.addProduct("product 00", "add product", 10.0, cat01);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, idProduct);
    }

    @Test
    void addExistingProduct() {
        Category cat01 = new Category("categorie 1", "descr 1");
        assertThrows(Exception.class, () -> {productDAO.addProduct("product 01", "add product", 10.0, cat01);}, "This product name already exist.");
    }
    @Test
    void addProductNotExistingCategory() {

        Category cat01 = new Category("categorie -1", "descr 1");
        assertThrows(Exception.class, () -> {productDAO.addProduct("product 01", "add product", 10.0, cat01);},
                "This category does not exist.");
    }
    @Test
    void addProductCategoryNull() {
        assertThrows(Exception.class, () -> {productDAO.addProduct("product 01", "add product", 10.0, null);},
                "The product category has to be set.");
    }

    @Test
    void updateProduct() {
        int idProduct = -1;
        Category cat01 = new Category("categorie 1", "descr 1");
        try {
            idProduct = productDAO.updateProduct("product to update", "product update",  "description", 10, cat01);

        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, idProduct);

    }

    @Test
    void updateProductNameExisting() {
        Category cat01 = new Category("categorie 1", "descr 1");
        assertThrows(Exception.class, () ->{
            productDAO.updateProduct("product 01", "product 02",  "description", 10, cat01);},
                "The new name refer to an existing product." );
    }

    @Test
    void updateProductNullCategory() {
        Category cat01 = new Category("categorie 1", "descr 1");
        assertThrows(Exception.class, () ->{
                    productDAO.updateProduct("product 01", "product 02",  "description", 10, null);},
                "The product category has to be set." );
    }

    @Test
    void updateProductNotExistingCategory() {
        Category cat01 = new Category("categorie -1", "descr 1");
        assertThrows(Exception.class, () ->{
                    productDAO.updateProduct("product 01", "product 02",  "description", 10, cat01);},
                "This category does not exist." );
    }

    @Test
    void updateProductNotExisting() {
        Category cat01 = new Category("categorie 1", "descr 1");
        assertThrows(Exception.class, () ->{
                    productDAO.updateProduct("product -01", "product --01",  "description", 10, cat01);},
                "This name doesn't refer to an existing product." );
    }

    @Test
    void deleteProduct() {
        boolean b = false;
        Category cat01 = new Category("categorie 1", "descr 1");
        try {
            b = productDAO.deleteProduct("product to delete");

        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertTrue(b);
    }

    @Test
    void deleteProductNotExisting() {
        assertThrows(Exception.class, () ->{
                    productDAO.deleteProduct("product to delete -1");},
                "This name doesn't refer to an existing product." );
    }
    @Test
    void deleteProductNotEmpty() {
        assertThrows(Exception.class, () ->{
                    productDAO.deleteProduct("product not empty");},
                "This product still has items." );
    }


}