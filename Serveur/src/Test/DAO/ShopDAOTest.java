package Test.DAO;

import DAO.ShopDAO;
import DAO.UserDAO;
import Model.Category;
import Model.Shop;
import Model.User;
import Model.UserType;
import Test.Mock.UserDAOMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ShopDAOTest {
    ShopDAO shopDAO;

    @BeforeEach
    void setUp() {
        UserDAOMock userDAO = new UserDAOMock();
        shopDAO = new ShopDAO(userDAO);
    }

    @Test
    void getShops() {
        List<Shop> shops = shopDAO.getShops();
        assertNotNull(shops);
        assertFalse(shops.isEmpty());
    }

    @Test
    void getShopById() {
        Shop shop = shopDAO.getShopById(1);
        assertNotNull(shop);
        assertEquals(1, shop.getId());
    }

    @Test
    void getNotExistingShopById() {
        Shop shop = shopDAO.getShopById(-1);
        assertNull(shop);
    }


    @Test
    void addShop() {
        int idShop = -1;
        User user = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);
        try{

            idShop = shopDAO.addShop("4 rue de test", "test", user);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, idShop);

    }


    @Test
    void updateShop() {

        int idCategory = -1;
        User user = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);

        try{

            idCategory = shopDAO.updateShop(2 , "adress update", "town update", user);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertNotEquals(-1, idCategory);
    }


    @Test
    void updateShopNotExisting() {
        User user = new User("email02@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);
        assertThrows(Exception.class, () -> {shopDAO.updateShop(-1 , "adress  update", "town update", user);},
                "This id doesn't refer to an existing shop.");
    }

    @Test
    void deleteShop() {

        boolean b = false;
        try {
            b = shopDAO.deleteShop(3);

        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertTrue(b);
    }

    @Test
    void deleteShopNotExisting() {
        assertThrows(Exception.class, () -> {shopDAO.deleteShop(-1);},
                "This id doesn't refer to an existing shop.");
    }

    @Test
    void deleteShopShelfNotEmpty() {
        assertThrows(Exception.class, () -> {shopDAO.deleteShop(4);},
                "This shop still has shelf.");
    }
}