package Test.DAO;

import DAO.UserDAO;
import Model.User;
import Model.UserType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserDAOTest {
    UserDAO userDAO;
    @BeforeEach
    void setUp() {
        userDAO = new UserDAO();
    }


    /**
     * Test si la liste d'utilisateur est bien retournée et qu'elle n'est pas vide
     */
    @Test
    void getUsers() {
        List<User> users = userDAO.getUsers();
        assertNotNull(users);
        assertFalse(users.isEmpty());
    }

    /**
     * Test l'obtention d'un utilisateur par son mail
     */
    @Test
    void getUserByEmail() {
        User user = userDAO.getUserByEmail("test01@test.fr");
        assertNotNull(user);
        assertEquals("test01@test.fr", user.getEmail());
    }

    /**
     * Test de non obtention d'un utilisateur non existant
     */
    @Test
    void getNotExistingUserByEmail() {
        User user = userDAO.getUserByEmail("test-1@test.fr");
        assertNull(user);
    }

    /**
     * test de l'ajout d'un d'un utilisateur
     */
    @Test
    void addUser() {
        int userId = -1;
        try{
            userId = userDAO.addUser("test00@test.fr", "test", "test ajout", "test", UserType.REGISTERED);
        }
        catch (Exception e){

            fail(e.getMessage());
        }
        assertNotEquals(-1, userId);
    }


    /**
     * Test de l'ajout d'un utilisateur ayant un mail deja present dans la base de données
     */
    @Test
    void addExistingUser() {
        assertThrows(Exception.class, () ->{userDAO.addUser("test01@test.fr", "test",
                "Test 01", "Test", UserType.REGISTERED);}, "This user email already exist.");

    }


    /**
     * Test de mise à jour d'un utilisateur avec une adresse inexistante dans la base de données
     */
    @Test
    void updateExistingUser() {
        int userId = -1;
        try{
            userId = userDAO.updateUser("test03@test.fr", "test-03@test.fr", "test", "test 03 update",
                    "test", UserType.ADMIN);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertEquals(3, userId);
    }

    /**
     * Test de mise à jour d'un utilisateur avec les une adresse mail ne lui appartenant pas et presente dans la base de données
     */
    @Test
    void updateExistingUserAndMail() {
            assertThrows(Exception.class, () ->{userDAO.updateUser("test04@test.fr", "test01@test.fr",
                    "test", "test 03 update","test", UserType.ADMIN);},"This email refer to an existing user");

    }

    /**
     * Test de mise à jour d'un utilisateur avec les une adresse mail lui appartenant
     */
    @Test
    void updateExistingUserAndOwnMail() {
        int userId = -1;
        try{
            userId = userDAO.updateUser("test04@test.fr", "test04@test.fr", "test", "test 04 update",
                    "test", UserType.ADMIN);
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertEquals(4, userId);
    }

    /**
     * Test de modification d'un utilisateur non existant
     */
    @Test
    void updateNotExistingUser() {
        assertThrows(Exception.class, () ->{userDAO.updateUser("test-01@test.fr", "test01@test.fr",
            "test", "test 03 update","test", UserType.ADMIN);},"This email doesn't refer to an exiting user.");

    }

    /**
     * test de suppression d'un utilisateur existant
     */
    @Test
    void deleteUser() {
        boolean b = false;
        try {
            b = userDAO.deleteUser("test05@test.fr");
        }
        catch (Exception e){
            fail(e.getMessage());
        }
        assertTrue(b);
    }

    /**
     * test de suppresion d'un utilisateur inexistant
     */
    @Test
    void deleteNotExistingUser() {

        assertThrows(Exception.class, () ->{userDAO.deleteUser("test-05@test.fr");},"This email doesn't refer to an exiting user.");
    }

    /**
     * test de suppression d'un utilisateur administrateur
     */
    @Test
    void deleteUserAdmin() {

        assertThrows(Exception.class, () ->{userDAO.deleteUser("test06@test.fr");},"The admin can't be deleted.");
    }
}