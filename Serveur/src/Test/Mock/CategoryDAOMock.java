package Test.Mock;

import DAO.CategoryDAO;
import Model.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoryDAOMock extends CategoryDAO {

    private List<Category> categories ;


    public CategoryDAOMock(){
        int i =0;
        Category cat01 = new Category("categorie 1", "descr 1");
        cat01.setId(0);
        cat01.setProducts(new ArrayList<>());

        Category  cat02 = new Category("not empty category", "desc 02");
        cat02.setId(1);
        cat02.setProducts(new ArrayList<>());

        Category  cat03 = new Category("category to delete with product", "desc 02");
        cat03.setId(4);
        cat03.setProducts(new ArrayList<>());

        categories= new ArrayList<>(Arrays.asList(cat01,cat02, cat03));
    }

    @Override
    public Category getCategoryByName(String name) {
        Category category = null;
        for(Category categoryElem : categories){
            if(categoryElem.getName().equals(name)){
                category = categoryElem;
            }
        }
        return category;
    }

    @Override
    public Integer addCategory(String name, String description) throws Exception {
        for(Category categoryElem : categories){
            if(categoryElem.getName().equals(name)){
                throw new Exception("This category name already exist.");
            }
        }
        categories.add(new Category(name, description));
        return categories.size() - 1;
    }

    @Override
    public Integer updateCategory(String currentName, String newName, String description) throws Exception {

        Category category = null;
        for(Category categoryElem : categories){
            if(categoryElem.getName().equals(currentName)){
                category = categoryElem;
            }
            if(categoryElem.getName().equals(newName)){
                throw new Exception("This name refer to an existing category.");
            }
        }
        if(category == null){
            throw new Exception("This name doesn't refer to an existing category.");
        }
        category.setName(newName);
        category.setDescription(description);
        category.setId(categories.size() );
        categories.add(category);
        return category.getId();
    }

    @Override
    public List<Category> getCategories() {
        return categories;
    }

    @Override
    public boolean deleteCategory(String name) throws Exception {
        Category category = null;
        for(Category categoryElem : categories){
            if(categoryElem.getName().equals(name)){
                category = categoryElem;

                if(!category.getProducts().isEmpty()){
                    throw new Exception("This category still has products.");
                }
            }
        }
        if(category == null){
            throw new Exception("This name doesn't refer to an existing category.");
        }
        return categories.remove(category);
    }
}
