package Test.Mock;

import DAO.ShopDAO;
import DAO.UserDAO;
import Model.Shop;
import Model.User;
import Model.UserType;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShopDAOMock extends ShopDAO {

    private List<Shop> shopList;


    /**
     * The constructor of the shop dao
     * Initialize the list of shop
     * Load data from database*
     */
    public ShopDAOMock() {
        UserDAOMock userDAOMock = new UserDAOMock();
        User user01 = userDAOMock.getUserByEmail("email06@test.fr");
        Shop shop01 = new Shop( "address 01",  "town 01", user01);
        shop01.setId(1);
        Shop shop02 = new Shop( "address 02",  "town 01", user01);
        shop02.setId(2);
        Shop shop03 = new Shop( "address 03",  "town 02", user01);
        shop03.setId(3);

        user01.getShops().add(shop01);
        user01.getShops().add(shop02);
        user01.getShops().add(shop03);

        shopList = new ArrayList<>(Arrays.asList(shop01, shop02, shop03));
    }


    @Override
    public boolean deleteShop(int id) throws Exception {
        Shop shop = null;
        for(Shop shopElem : shopList){
            if(shopElem.getId().equals(id)){
                if(!shopElem.getShelfs().isEmpty()){

                    throw new Exception("This shop still has shelf.");
                }

                shop = shopElem;
            }
        }
        if(shop == null){
            throw new Exception("This id doesn't refer to an existing shop.");
        }
        return shopList.remove(shop);
    }

    @Override
    public Integer addShop(String address, String town, User manager) {
        Shop shop01 = new Shop( address,  town, manager);
        shop01.setId(shopList.size());
        shopList.add(shop01);
        return shop01.getId();
    }

    @Override
    public Integer updateShop(int id, String address, String town, User manager) throws Exception {
        Shop shop = null;
        for(Shop shopElem : shopList){
            if(shopElem.getId() == id){
                shop = shopElem;
            }
        }
        if(shop == null){
            throw new Exception("This id doesn't refer to an existing shop.");
        }
        shop.setAddress(address);
        shop.setManager(manager);
        shop.setTown(town);
        return shop.getId();
    }

    @Override
    public List<Shop> getShops() {
        return shopList;
    }

    @Override
    public Shop getShopById(int id) {
        Shop shop = null;
        for(Shop shopElem : shopList){
            if(shopElem.getId() == id){
                shop = shopElem;
            }
        }
        return shop;
    }
}
