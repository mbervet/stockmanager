package Test.Mock;

import DAO.ShelfDAO;
import Model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShelfDAOMock extends ShelfDAO {

    List<Shelf> shelves;

    public ShelfDAOMock(){
        UserDAOMock userDAOMock = new UserDAOMock();
        ShopDAOMock shopDAOMock = new ShopDAOMock();

        User user02 = userDAOMock.getUserByEmail("email02@test.fr");

        Shop shop03 = shopDAOMock.getShopById(3);

        Shelf shelf01 = new Shelf(user02, shop03);
        shelf01.setId(1);
        shelf01.setItems(new ArrayList<>());
        shop03.getShelfs().add(shelf01);
        Shelf shelf02 = new Shelf(user02, shop03);
        shelf02.setId(2);
        shelf02.setItems(new ArrayList<>());
        shop03.getShelfs().add(shelf02);

        shelves = new ArrayList<>(Arrays.asList(shelf01, shelf02));
    }


    @Override
    public boolean deleteShelf(int id) throws Exception {
        Shelf shelf = null;
        for(Shelf shelfElem : shelves){
            if(shelfElem.getId() == id){
                shelf = shelfElem;

                if(!shelf.getItems().isEmpty()){
                    throw new Exception("This shelf still has items.");
                }
            }
        }
        if(shelf == null){
            throw new Exception("This id doesn't refer to an existing shelf.");
        }
        return shelves.remove(shelf);
    }

    @Override
    public Integer addShelf(User manager, Shop shop) throws Exception {

        if (shop == null){
            throw new Exception("The shelf shop has to be set.");
        }

        Shelf shelf02 = new Shelf(manager, shop);
        shelf02.setId(shelves.size());
        return shelf02.getId();
    }

    @Override
    public Integer updateShelf(int id, User manager, Shop shop) throws Exception {
        Shelf shelf = getShelfById(id);

        if (shop == null){
            throw new Exception("The shelf shop has to be set.");
        }
        if (shelf == null){
            throw new Exception("This id doesn't refer to an existing shelf.");
        }

        shelf.setManager(manager);
        shelf.setShop(shop);

        return shelf.getId();
    }


    @Override
    public List<Shelf> getShelfs() {
        return shelves;
    }

    @Override
    public Shelf getShelfById(int id) {
        Shelf shelf = null;
        for(Shelf shelfElem : shelves){
            if(shelfElem.getId() == id) {
                shelf = shelfElem;
            }
        }
        return shelf;
    }
}
