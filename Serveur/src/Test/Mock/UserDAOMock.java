package Test.Mock;

import DAO.UserDAO;
import Model.Category;
import Model.User;
import Model.UserType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserDAOMock extends UserDAO {

    private List<User> users;

    public UserDAOMock(){
        User user01 = new User("test06@test.fr", "password", "firstName", "lastName", UserType.ADMIN);
        user01.setShops(new ArrayList<>());
        user01.setShelfs(new ArrayList<>());
        User user02 = new User("test04@test.fr", "password", "firstName", "lastName", UserType.SHOP_MANAGER);
        user02.setShops(new ArrayList<>());
        user02.setShelfs(new ArrayList<>());
        users = new ArrayList<>(Arrays.asList(user01, user02));

    }

    @Override
    public List<User> getUsers() {
        return users;
    }

    @Override
    public boolean deleteUser(String email) throws Exception {
        User user = null;
        for(User user1 : users){
            if(user1.getEmail().equals(email)){
                user = user1;

                if(!user.getType().equals(UserType.ADMIN)){
                    throw new Exception("The admin can't be deleted.");
                }
            }
        }
        if(user == null){
            throw new Exception("This email doesn't refer to an exiting user.");
        }
        return users.remove(user);
    }

    @Override
    public Integer addUser(String email, String password, String firstName, String lastName, UserType type) throws Exception {
        User user = new User( email, password, firstName, lastName, type);
        for(User userElem : users){
            if(userElem.getEmail().equals(email)){
                throw new Exception("This user email already exist.");
            }
        }
        users.add(user);
        return users.size() - 1;
    }

    @Override
    public Integer updateUser(String currentEmail, String newEmail, String password, String firstName, String lastName, UserType type) throws Exception {
        User user = null;
        for(User categoryElem : users){
            if(categoryElem.getEmail().equals(currentEmail)){
                user = categoryElem;
            }
            if(categoryElem.getEmail().equals(newEmail)){
                throw new Exception("This email refer to an existing user");
            }
        }
        if(user == null){
            throw new Exception("This email doesn't refer to an exiting user.");
        }
        user.setEmail(newEmail);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setType(type);
        user.setId(users.size() );
        return user.getId();
    }

    @Override
    public User getUserByEmail(String email) {
        User user = null;
        for(User userElem : users){
            if(userElem.getEmail().equals(email)){
                user = userElem;
            }
        }
        return user;
    }


}
