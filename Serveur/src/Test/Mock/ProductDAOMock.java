package Test.Mock;

import DAO.ProductDAO;
import Model.Category;
import Model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductDAOMock extends ProductDAO {

    private List<Product> products;
    private CategoryDAOMock categoryDAOMock;


    public ProductDAOMock(){
        this.categoryDAOMock = new CategoryDAOMock();
        Category cat01 = categoryDAOMock.getCategoryByName("not empty category");

        Product product01 = new Product("product not empty","description", 10, cat01);
        cat01.getProducts().add(product01);
        product01.setItems(new ArrayList<>());
        Product product02 = new Product("name 02",  "description", 11, cat01);
        product02.setItems(new ArrayList<>());
        cat01.getProducts().add(product02);

        products = new ArrayList<>(Arrays.asList(product01, product02));

    }

    @Override
    public boolean deleteProduct(String name) throws Exception {
        Product product = null;
        for(Product productElem : products){
            if(productElem.getName().equals(name)){
                product = productElem;

                if(!product.getItems().isEmpty()){
                    throw new Exception("This product still has items.");
                }
            }
        }
        if(product == null){
            throw new Exception("This name doesn't refer to an existing product.");
        }
        return products.remove(product);
    }

    @Override
    public Integer addProduct(String name, String description, double price, Category category) throws Exception {
        for(Product product : products){
            if(product.getName().equals(name)){
                throw new Exception("This product name already exist.");
            }
        }
        products.add(new Product(name, description, price,category));
        return products.size() - 1;
    }

    @Override
    public Integer updateProduct(String currentName, String newName, String description, double price, Category category) throws Exception {

        Product product = null;
        for(Product productElem : products){
            if(productElem.getName().equals(currentName)){
                product = productElem;
            }
            if(productElem.getName().equals(newName)){
                throw new Exception("This product name already exist.");
            }
        }
        if(product == null){
            throw new Exception("This name doesn't refer to an existing product.");
        }
        if(categoryDAOMock.getCategoryByName(category.getName()) == null) {
            throw new Exception("This category does not exist.");

        }
        product.setName(newName);
        product.setDescription(description);
        product.setCategory(category);
        category.getProducts().add(product);
        product.setPrice(price);
        product.setId(products.size() );
        products.add(product);
        return product.getId();
    }

    @Override
    public List<Product> getProducts() {
        return products;
    }

    @Override
    public Product getProductByName(String name) {

        Product product = null;
        for(Product productElem : products){
            if(productElem.getName().equals(name)){
                product = productElem;
            }
        }
        return product;
    }

}
