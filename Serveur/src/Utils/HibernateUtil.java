package Utils;

import org.hibernate.*;
import org.hibernate.cfg.*;

/**
 * Management class for hibernate connection
 * @author mbervet
 */
public final class HibernateUtil {

	/**
	 * The session factory allowing to create hibernate session
	 */
	private static SessionFactory sessionFactory;

	/**
	 * The static constructor for hibernate session factory initialization
	 * @throws ExceptionInInitializerError if an error occurred during session factory initialization
	 */
	static {
		try {
			sessionFactory = new Configuration().configure().buildSessionFactory();
	    } 
		catch (Throwable ex) {
	       throw new ExceptionInInitializerError(ex);
	    }
	}

	/**
	 * Create a new hibernate session and begin a new transaction
	 * @return Session session : the new session
	 */
	public static Session BeginTransaction()
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		return session;
	}

	/**
	 * Commit the current transaction in the given session and close the session
	 * @param session : the session to use for commit
	 */
	public static void CommitTransaction(Session session)
	{
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * Shutdown the hibernate session factory
	 */
	public static void shutdown()
	{
		sessionFactory.close();
	}
}
