package Utils;

import java.util.logging.Level;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwa.AlgorithmConstraints.ConstraintType;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.ErrorCodes;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;

/**
 * This class allow to manage token identifier for connection system
 * @author mbervet
 */
public class TokenManagement {

    /**
     * The asymmetric key to crypt the token
     */
    private static RsaJsonWebKey rsaJsonWebKey = null;

    /**
     * The constructor of the token manager
     * Initialize the RSA key if it's null
     * @throws JoseException if the RSA key initialization failed
     */
    public TokenManagement() throws JoseException
    {
        if(rsaJsonWebKey == null)
        {
            try
            {
                rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
                rsaJsonWebKey.setKeyId("k1");
            }
            catch(JoseException e)
            {
                rsaJsonWebKey = null;
                throw e;
            }
        }
    }

    /**
     * Generate a new crypted token with the given user email
     * @param userEmail : the user email to put in the token
     * @return String token : the new token available for 1 hour
     * @throws JoseException if the generation failed
     */
    public String generateToken(String userEmail) throws JoseException
    {
        JwtClaims claims = new JwtClaims();
        claims.setIssuer("Issuer");
        claims.setAudience("Audience");
        claims.setExpirationTimeMinutesInTheFuture(60);
        claims.setGeneratedJwtId();
        claims.setIssuedAtToNow();
        claims.setNotBeforeMinutesInThePast(2);
        claims.setSubject("user");
        claims.setClaim("userEmail", userEmail);
        
        JsonWebSignature jws = new JsonWebSignature();
        jws.setKey(rsaJsonWebKey.getPrivateKey());
        jws.setPayload(claims.toJson());
        jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());        
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
        
        return jws.getCompactSerialization();
    }

    /**
     * Check if the given string correspond to an available token an decrypt it if it's the  case
     * Check if the token is still available and extract the user email if it's the case
     * @param jwt : the token to check
     * @return String userEmail : the user email registered in the token
     * @throws Exception if the given token it's not valid or if it's no more available or simply in case of failure
     */
    public String verifyToken(String jwt) throws Exception {
        String userEmail = null;
        
        JwtConsumerBuilder builder = new JwtConsumerBuilder();
        builder.setRequireExpirationTime();
        builder.setAllowedClockSkewInSeconds(30);
        builder.setRequireSubject();
        builder.setExpectedIssuer("Issuer");
        builder.setExpectedAudience("Audience");
        builder.setVerificationKey(rsaJsonWebKey.getKey());
        builder.setJwsAlgorithmConstraints(new AlgorithmConstraints(ConstraintType.WHITELIST, AlgorithmIdentifiers.RSA_USING_SHA256));
       
        JwtConsumer jwtConsumer = builder.build();

        try
        {
            JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
            userEmail = jwtClaims.getClaimValue("userEmail").toString();
        }
        catch (InvalidJwtException e)
        {
            if (e.hasExpired())
            {
                try {
                    throw new Exception("JWT expired at " + e.getJwtContext().getJwtClaims().getExpirationTime());
                } catch (MalformedClaimException ex) {
                    java.util.logging.Logger.getLogger(TokenManagement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (e.hasErrorCode(ErrorCodes.AUDIENCE_INVALID))
            {
                try {
                    throw new Exception("JWT had wrong audience: " + e.getJwtContext().getJwtClaims().getAudience());
                } catch (MalformedClaimException ex) {
                    java.util.logging.Logger.getLogger(TokenManagement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        return userEmail;
    }
}
