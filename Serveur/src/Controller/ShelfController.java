package Controller;

import DAO.MainDAO;
import Model.Shelf;
import Model.Shop;
import Model.User;
import Model.UserType;

import java.util.List;

/**
 * The shelf controller
 * Managing access and dao for all shelf
 * @author mbervet
 */
public class ShelfController {

    /**
     * A reference to the main dao
     */
    private DAO.MainDAO MainDAO;

    /**
     * A reference to the user controller to check connection
     */
    private UserController UserController;

    /**
     * The constructor of shelf controller
     * @param mainDAO : the reference to the main doa
     * @param userController : the reference to the user controller
     */
    public ShelfController(MainDAO mainDAO, UserController userController){
        MainDAO = mainDAO;
        UserController = userController;
    }

    /**
     * Create a new shelf and add it to the local instance and in the database
     * @param connectedEmail : the email of the connected user
     * @param managerEmail : the shelf manager email
     * @param shopId : the shelf shop id
     * @return boolean added : true if the shelf is successfully added, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean addShelf(String connectedEmail, String managerEmail, Integer shopId) throws Exception {
        boolean added = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        User manager = MainDAO.getUserDAO().getUserByEmail(managerEmail);
        Shop shop = MainDAO.getShopDAO().getShopById(shopId);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && connectedUser.getType() != UserType.SHOP_MANAGER){
            throw new Exception("You don't have the rights to add a new shop.");
        }
        if (connectedUser.getType() == UserType.SHOP_MANAGER && !connectedUser.getEmail().equals(shop.getManager().getEmail())){
            throw new Exception("You are not the manager of this shop.");
        }

        if (MainDAO.getShelfDAO().addShelf(manager, shop) != null){
            added = true;
        }

        return added;
    }

    /**
     * Return the list of all shelf
     * @param connectedEmail : the email of the connected user
     * @return List<Shelf> Shelfs : the list of shelf
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public List<Shelf> getShelfList(String connectedEmail) throws Exception {
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() == UserType.REGISTERED){
            throw new Exception("You don't have access to shelf list.");
        }

        return MainDAO.getShelfDAO().getShelfs();
    }

    /**
     * Return the list of all shelf in the shop corresponding to the given id
     * @param connectedEmail : the email of the connected user
     * @param shopId : the shop id to look for
     * @return List<Shelf> Shelfs : the list of shelf
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public List<Shelf> getShelfListByShop(String connectedEmail, Integer shopId) throws Exception {
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() == UserType.REGISTERED){
            throw new Exception("You don't have access to shelf list.");
        }

        return MainDAO.getShopDAO().getShopById(shopId).getShelfs();
    }

    /**
     * Return the information of the shelf corresponding to the given id
     * @param connectedEmail : the email of the connected user
     * @param shelfId : the shop id to look for
     * @return Shelf shelf : the corresponding shelf or null
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public Shelf getShelfInformationById(String connectedEmail, Integer shelfId) throws Exception {
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() == UserType.REGISTERED){
            throw new Exception("You don't have access to shelf information.");
        }

        return MainDAO.getShelfDAO().getShelfById(shelfId);
    }

    /**
     * Change the manager of the shelf corresponding to the given id
     * @param connectedEmail : the email of the connected user
     * @param shelfId : the shop id to look for
     * @param mangerEmail : the new shelf manager email
     * @return boolean changed : true if the shelf manager is successfully changed, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean changeManager(String connectedEmail, Integer shelfId, String mangerEmail) throws Exception {
        boolean changed = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        Shelf shelf = MainDAO.getShelfDAO().getShelfById(shelfId);
        User manager = MainDAO.getUserDAO().getUserByEmail(mangerEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && connectedUser.getType() != UserType.SHOP_MANAGER){
            throw new Exception("You don't have the rights to change the shelf manager.");
        }

        if (MainDAO.getShelfDAO().updateShelf(shelf.getId(), manager, shelf.getShop()) != null){
            changed = true;
        }

        return changed;
    }

    /**
     * Change the shop of the shelf corresponding to the given id
     * @param connectedEmail : the email of the connected user
     * @param shelfId : the shop id to look for
     * @param shopId : the new shelf shop
     * @return boolean changed : true if the shelf shop is successfully changed, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean changeShop(String connectedEmail, Integer shelfId, Integer shopId) throws Exception {
        boolean changed = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        Shelf shelf = MainDAO.getShelfDAO().getShelfById(shelfId);
        Shop shop = MainDAO.getShopDAO().getShopById(shopId);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && connectedUser.getType() != UserType.SHOP_MANAGER){
            throw new Exception("You don't have the rights to change the shelf shop.");
        }

        if (MainDAO.getShelfDAO().updateShelf(shelf.getId(), shelf.getManager(), shop) != null){
            changed = true;
        }

        return changed;
    }

    /**
     * Remove the shelf corresponding to the given id from the local instance and from the database
     * @param connectedEmail : the email of the connected user
     * @param shelfId : the shop id to look for
     * @return boolean deleted : true if the shelf is successfully deleted, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean deleteShelf(String connectedEmail, Integer shelfId) throws Exception {
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && connectedUser.getType() != UserType.SHOP_MANAGER){
            throw new Exception("You don't have the rights to delete a shelf.");
        }

        return MainDAO.getShelfDAO().deleteShelf(shelfId);
    }
}
