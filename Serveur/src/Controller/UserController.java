package Controller;

import DAO.MainDAO;
import Model.User;
import Model.UserType;

import java.util.ArrayList;
import java.util.List;

/**
 * The user controller
 * Managing access and dao for all users
 * @author mbervet
 */
public class UserController {

    /**
     * A reference to the main dao
     */
    private MainDAO MainDAO;

    /**
     * The list of users currently connected to manage access restriction
     */
    private List<User> ConnectedUsers;

    /**
     * The constructor of the user controller
     * Get a reference to the main dao and initialize the list of connected users
     * @param mainDAO : a reference to the main dao
     */
    public UserController(MainDAO mainDAO){
        MainDAO = mainDAO;
        ConnectedUsers = new ArrayList<>();
    }

    /**
     * Check if the user corresponding to the given email is connected
     * @param email : the user email to look for
     * @return User user : the connected user
     * @throws Exception if the given email doesn't match to an existing user or if the user is not in the connected list
     */
    User isConnected(String email) throws Exception {
        User user = MainDAO.getUserDAO().getUserByEmail(email);

        if (user == null){
            throw new Exception("The given email doesn't refer to an existing user.");
        }
        if (!ConnectedUsers.contains(user)){
            throw new Exception("You have to be connected to have access to a service");
        }

        return user;
    }

    /**
     * This method allow user registration with the given information
     * @param email : the user email
     * @param password : the user password
     * @param firstName : the user first name
     * @param lastName : the user last name
     * @return boolean registered : true if the user is successfully registered, false otherwise
     * @throws Exception if the email is already used
     */
    public boolean register(String email, String password, String firstName, String lastName) throws Exception {
        boolean registered = false;

        if (MainDAO.getUserDAO().addUser(email, password, firstName, lastName, UserType.REGISTERED) != null){
            registered = true;
        }

        return registered;
    }

    /**
     * Connect a user if his authorised
     * @param email : the user email to check
     * @param password : the user password to check
     * @return boolean connected : true if the user is successfully connected, false otherwise
     * @throws Exception if the given email doesn't match to an existing user
     */
    public boolean signIn(String email, String password) throws Exception {
        boolean connected = false;
        User user = MainDAO.getUserDAO().getUserByEmail(email);

        if (user == null){
            throw new Exception("The given email doesn't refer to an existing user.");
        }

        if (user.getPassword().equals(password)){
            ConnectedUsers.add(user);
            connected = true;
        }

        return connected;
    }

    /**
     * Disconnect the user corresponding to the given email
     * @param email : the user email to look for
     * @return boolean disconnected : true if the user is successfully disconnected, false otherwise
     * @throws Exception if the user is not connected
     */
    public boolean signOut(String email) throws Exception {
        User connectedUser = isConnected(email);
        return ConnectedUsers.remove(connectedUser);
    }

    /**
     * Return the list of all users
     * @param connectedEmail : the email of the connected user
     * @return List<User> Users : the list of all users
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public List<User> getUserList(String connectedEmail) throws Exception {
        User connectedUser = isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && connectedUser.getType() != UserType.SHOP_MANAGER){
            throw new Exception("You can't access to the user list.");
        }

        return MainDAO.getUserDAO().getUsers();
    }

    /**
     * Return the user corresponding to the given email
     * @param connectedEmail : the email of the connected user
     * @param emailToFound : the email to look for
     * @return User user : the user corresponding to given email or null
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public User getUserInformationByEmail(String connectedEmail, String emailToFound) throws Exception {
        User connectedUser = isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && connectedUser.getType() != UserType.SHOP_MANAGER){
            if (!connectedUser.getEmail().equals(emailToFound)){
                throw new Exception("You don't have access to user information.");
            }
        }

        return MainDAO.getUserDAO().getUserByEmail(emailToFound);
    }

    /**
     * Modify user information with the given data
     * @param connectedEmail : the email of the connected user
     * @param currentEmail : the current email of the user
     * @param newEmail : the new email of the user
     * @param password : the new password of the user
     * @param firstName : the new first name
     * @param lastName : the new last name
     * @return boolean modified : true if the user is successfully modified, false otherwise
     * @throws Exception if the user is not connected or if the user doesn't have the rights
     */
    public boolean modifyUserInformation(String connectedEmail, String currentEmail, String newEmail, String password, String firstName, String lastName) throws Exception {
        boolean modified = false;
        User connectedUser = isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can modified users information");
        }

        if (MainDAO.getUserDAO().updateUser(currentEmail, newEmail, password, firstName, lastName, MainDAO.getUserDAO().getUserByEmail(currentEmail).getType()) != null){
            modified = true;
        }

        return modified;
    }

    /***
     * Promote a user to the next post if he's not too high
     * @param connectedEmail : the email of the connected user
     * @param emailToPromote : the email of the user to promote
     * @return boolean promoted : true if the user is successfully promote, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean promote(String connectedEmail, String emailToPromote) throws Exception {
        boolean promoted = false;
        User connectedUser = isConnected(connectedEmail);
        User user = MainDAO.getUserDAO().getUserByEmail(emailToPromote);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && connectedUser.getType() != UserType.SHOP_MANAGER){
            throw new Exception("You don't have the right to promote users.");
        }
        if (user.getType() == UserType.CEO || (user.getType() == UserType.SHOP_MANAGER && connectedUser.getType() != UserType.ADMIN)){
            throw new Exception("You can't promote " + user.getType() + ".");
        }
        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && user.getType() == UserType.SHELF_MANAGER){
            throw new Exception("Only the CEO can promote to shop manager");
        }

        UserType type = user.getType();
        if (type == UserType.REGISTERED){
            type = UserType.SHELF_MANAGER;
        }
        else if (type == UserType.SHELF_MANAGER){
            type = UserType.SHOP_MANAGER;
        }
        else if (type == UserType.SHOP_MANAGER){
            type = UserType.CEO;
        }

        if (MainDAO.getUserDAO().updateUser(user.getEmail(), user.getEmail(), user.getPassword(), user.getFirstName(), user.getLastName(), type) != null){
            promoted = true;
        }

        return promoted;
    }

    /**
     * Revoke the rights of a user
     * @param connectedEmail : the email of the connected user
     * @param emailToRevoke : the email of the user to revoke rights
     * @return boolean revoked : true if the rights has been revoked, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean revokeRights(String connectedEmail, String emailToRevoke) throws Exception {
        boolean revoked = false;
        User connectedUser = isConnected(connectedEmail);
        User user = MainDAO.getUserDAO().getUserByEmail(emailToRevoke);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO && connectedUser.getType() != UserType.SHOP_MANAGER){
            throw new Exception("You don't have the right to revoke rights.");
        }
        if (user.getType() == UserType.ADMIN && user.getType() == UserType.CEO){
            throw new Exception("You can't revoke the rights of the " + user.getType() + ".");
        }
        if (connectedUser.getType() == UserType.SHOP_MANAGER && user.getType() == UserType.SHOP_MANAGER){
            throw new Exception("A shop manager can't revoke the rights of another.");
        }

        if (MainDAO.getUserDAO().updateUser(user.getEmail(), user.getEmail(), user.getPassword(), user.getFirstName(), user.getLastName(), UserType.REGISTERED) != null){
            revoked = true;
        }

        return revoked;
    }

    /**
     * Fired a user
     * @param connectedEmail : the email of the connected user
     * @param emailToFired : the email of the user to fired
     * @return boolean fired : true if the user is successfully fired, false otherwise
     * @throws Exception if the user is not connected or if the user doesn't have the rights
     */
    public boolean firedUser(String connectedEmail, String emailToFired) throws Exception {
        User connectedUser = isConnected(connectedEmail);
        User user = MainDAO.getUserDAO().getUserByEmail(emailToFired);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can fired users.");
        }
        if (user.getType() == UserType.CEO){
            throw new Exception("The ceo can't be fired.");
        }

        ConnectedUsers.remove(user);

        return MainDAO.getUserDAO().deleteUser(emailToFired);
    }
}
