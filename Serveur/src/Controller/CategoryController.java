package Controller;

import DAO.MainDAO;
import Model.Category;
import Model.User;
import Model.UserType;

import java.util.List;

/**
 * The category controller
 * Managing access and dao for all categories
 * @author mbervet
 */
public class CategoryController {

    /**
     * A reference to the main dao
     */
    private DAO.MainDAO MainDAO;

    /**
     * A reference to the user controller to check connection
     */
    private UserController UserController;

    /**
     * The constructor of category controller
     * @param mainDAO : the reference to the main doa
     * @param userController : the reference to the user controller
     */
    public CategoryController(MainDAO mainDAO, UserController userController){
        MainDAO = mainDAO;
        UserController = userController;
    }

    /**
     * Create a new category and add it to the local instance and in the database
     * @param connectedEmail : the email of the connected user
     * @param name : the category name
     * @param description : the category description
     * @return boolean added : true if the category is successfully added, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean addCategory(String connectedEmail, String name, String description) throws Exception {
        boolean added = false;
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can add a new category.");
        }

        if (MainDAO.getCategoryDAO().addCategory(name, description) != null){
            added = true;
        }

        return added;
    }

    /**
     * Return the list of all categories
     * @param connectedEmail : the email of the connected user
     * @return List<Category> Categories : the list of category
     * @throws Exception if the user is not connected
     */
    public List<Category> getCategoryList(String connectedEmail) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getCategoryDAO().getCategories();
    }

    /**
     * Return the information of the category corresponding to the given name
     * @param connectedEmail : the email of the connected user
     * @param name : the name to look for
     * @return Category category : the corresponding category or null
     * @throws Exception if the user is not connected
     */
    public Category getCategoryInformation(String connectedEmail, String name) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getCategoryDAO().getCategoryByName(name);
    }

    /**
     * Modify category information with the given data
     * @param connectedEmail : the email of the connected user
     * @param currentName : the current name of the category
     * @param newName : the new category name
     * @param description : the new category description
     * @return boolean modified : true if the category is successfully modified, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean modifyCategoryInformation(String connectedEmail, String currentName, String newName, String description) throws Exception {
        boolean modified = false;
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can modify categories information.");
        }

        if (MainDAO.getCategoryDAO().updateCategory(currentName, newName, description) != null){
            modified = true;
        }

        return modified;
    }

    /**
     * Remove the category corresponding to the given name from the local instance and from the database
     * @param connectedEmail : the email of the connected user
     * @param name : the name to look for
     * @return boolean deleted : true if the category is successfully deleted, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean deleteCategory(String connectedEmail, String name) throws Exception {
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can delete a category.");
        }

        return MainDAO.getCategoryDAO().deleteCategory(name);
    }
}
