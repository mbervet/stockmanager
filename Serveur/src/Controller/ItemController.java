package Controller;

import DAO.MainDAO;
import Model.*;

import java.util.List;

/**
 * The item controller
 * Managing access and dao for all categories
 * @author mbervet
 */
public class ItemController {

    /**
     * A reference to the main dao
     */
    private DAO.MainDAO MainDAO;

    /**
     * A reference to the user controller to check connection
     */
    private UserController UserController;

    /**
     * The constructor of item controller
     * @param mainDAO : the reference to the main doa
     * @param userController : the reference to the user controller
     */
    public ItemController(MainDAO mainDAO, UserController userController){
        MainDAO = mainDAO;
        UserController = userController;
    }

    /**
     * Create a new item and add it to the local instance and in the database
     * @param connectedEmail : the email of the connected user
     * @param code : the item code
     * @param shelfId : the item shelf id
     * @param productName : the item product name
     * @return boolean added : true if the item is successfully added, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean addItem(String connectedEmail, String code, Integer shelfId, String productName) throws Exception {
        boolean added = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        Shelf shelf = MainDAO.getShelfDAO().getShelfById(shelfId);
        Product product = MainDAO.getProductDAO().getProductByName(productName);

        if (connectedUser.getType() == UserType.REGISTERED){
            throw new Exception("You don't have the rights to add new articles.");
        }

        if (MainDAO.getItemDAO().addItem(code, shelf, product) != null){
            added = true;
        }

        return added;
    }

    /**
     * Return the list of all items
     * @param connectedEmail : the email of the connected user
     * @return List<Item> Items : the list of item
     * @throws Exception if the user is not connected
     */
    public List<Item> getItemList(String connectedEmail) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getItemDAO().getItems();
    }

    /**
     * Return the list of all items in the product corresponding to the given name
     * @param connectedEmail : the email of the connected user
     * @param productName : the product name to look for
     * @return List<Item> Items : the list of item
     * @throws Exception if the user is not connected
     */
    public List<Item> getItemListByProduct(String connectedEmail, String productName) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getProductDAO().getProductByName(productName).getItems();
    }

    /**
     * Return the information of the product corresponding to the given name
     * @param connectedEmail : the email of the connected user
     * @param code : the code to look for
     * @return Item item : the corresponding item or null
     * @throws Exception if the user is not connected
     */
    public Item getItemInformationByCode(String connectedEmail, String code) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getItemDAO().getItemByCode(code);
    }

    /**
     * Change the product of the item corresponding to the given code
     * @param connectedEmail : the email of the connected user
     * @param code : the code to look for
     * @param productName : the new item product name
     * @return boolean changed : true if the item product is successfully changed, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean changeProduct(String connectedEmail, String code, String productName) throws Exception {
        boolean changed = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        Product product = MainDAO.getProductDAO().getProductByName(productName);

        if (connectedUser.getType() == UserType.REGISTERED){
            throw new Exception("You don't have the rights to change items product.");
        }

        if (MainDAO.getItemDAO().updateItem(code, MainDAO.getItemDAO().getItemByCode(code).getShelf(), product) != null){
            changed = true;
        }

        return changed;
    }

    /**
     * Change the shelf of the item corresponding to the given code
     * @param connectedEmail : the email of the connected user
     * @param code : the code to look for
     * @param shelfId : the new item shelf id
     * @return boolean changed : true if the item shelf is successfully changed, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean changeShelf(String connectedEmail, String code, Integer shelfId) throws Exception {
        boolean changed = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        Shelf shelf = MainDAO.getShelfDAO().getShelfById(shelfId);

        if (connectedUser.getType() == UserType.REGISTERED){
            throw new Exception("You don't have the rights to change items shelf.");
        }

        if (MainDAO.getItemDAO().updateItem(code, shelf, MainDAO.getItemDAO().getItemByCode(code).getProduct()) != null){
            changed = true;
        }

        return changed;
    }

    /**
     * Remove the item corresponding to the given code from the local instance and from the database
     * @param connectedEmail : the email of the connected user
     * @param code : the code to look for
     * @return boolean deleted : true if the item is successfully deleted, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean deleteItem(String connectedEmail, String code) throws Exception {
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() == UserType.REGISTERED){
            throw new Exception("You don't have the rights to delete items.");
        }

        return MainDAO.getItemDAO().deleteItem(code);
    }
}
