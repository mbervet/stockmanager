package Controller;

import DAO.MainDAO;
import Model.Category;
import Model.Product;
import Model.User;
import Model.UserType;

import java.util.List;


/**
 * The category product
 * Managing access and dao for all products
 * @author mbervet
 */
public class ProductController {

    /**
     * A reference to the main dao
     */
    private DAO.MainDAO MainDAO;

    /**
     * A reference to the user controller to check connection
     */
    private UserController UserController;

    /**
     * The constructor of product controller
     * @param mainDAO : the reference to the main doa
     * @param userController : the reference to the user controller
     */
    public ProductController(MainDAO mainDAO, UserController userController){
        MainDAO = mainDAO;
        UserController = userController;
    }

    /**
     * Create a new product and add it to the local instance and in the database
     * @param connectedEmail : the email of the connected user
     * @param name : the product name
     * @param description : the product description
     * @param price : the product price
     * @param categoryName : the product category name
     * @return boolean added : true if the product is successfully added, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean addProduct(String connectedEmail, String name, String description, double price, String categoryName) throws Exception {
        boolean added = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        Category category = MainDAO.getCategoryDAO().getCategoryByName(categoryName);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can add new product.");
        }

        if (MainDAO.getProductDAO().addProduct(name, description, price, category) != null){
            added = true;
        }

        return added;
    }

    /**
     * Return the list of all products
     * @param connectedEmail : the email of the connected user
     * @return List<Product> Products : the list of product
     * @throws Exception if the user is not connected
     */
    public List<Product> getProductList(String connectedEmail) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getProductDAO().getProducts();
    }

    /**
     * Return the list of all products in the category corresponding to the given name
     * @param connectedEmail : the email of the connected user
     * @param categoryName : the category name to look for
     * @return List<Product> Products : the list of product
     * @throws Exception if the user is not connected
     */
    public List<Product> getProductListByCategory(String connectedEmail, String categoryName) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getCategoryDAO().getCategoryByName(categoryName).getProducts();
    }

    /**
     * Return the information of the product corresponding to the given name
     * @param connectedEmail : the email of the connected user
     * @param name : the name to look for
     * @return Product product : the corresponding product or null
     * @throws Exception if the user is not connected
     */
    public Product getProductInformationByName(String connectedEmail, String name) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getProductDAO().getProductByName(name);
    }

    /**
     * Change the category of the product corresponding to the given name 
     * @param connectedEmail : the email of the connected user
     * @param name : the name to look for
     * @param categoryName : the new product category name
     * @return boolean changed : true if the product category is successfully changed, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean changeCategory(String connectedEmail, String name, String categoryName) throws Exception {
        boolean changed = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        Product product = MainDAO.getProductDAO().getProductByName(name);
        Category category = MainDAO.getCategoryDAO().getCategoryByName(categoryName);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can change products category.");
        }

        if (MainDAO.getProductDAO().updateProduct(product.getName(), product.getName(), product.getDescription(), product.getPrice(), category) != null){
            changed = true;
        }

        return changed;
    }

    /**
     * Modify product information with the given data
     * @param connectedEmail : the email of the connected user
     * @param currentName : the current product name
     * @param newName : the new product name
     * @param description : the new product description
     * @param price : the new product price
     * @return boolean modified : true if the product is successfully modified, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean modifyProductInformation(String connectedEmail, String currentName, String newName, String description, double price) throws Exception {
        boolean modified = false;
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can modify products information.");
        }

        if (MainDAO.getProductDAO().updateProduct(currentName, newName, description, price, MainDAO.getProductDAO().getProductByName(currentName).getCategory()) != null){
            modified = true;
        }

        return modified;
    }

    /**
     * Remove the product corresponding to the given name from the local instance and from the database
     * @param connectedEmail : the email of the connected user
     * @param name : the name to look for
     * @return boolean deleted : true if the product is successfully deleted, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean deleteProduct(String connectedEmail, String name) throws Exception {
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can delete products.");
        }

        return MainDAO.getProductDAO().deleteProduct(name);
    }
}
