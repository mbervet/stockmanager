package Controller;

import DAO.MainDAO;
import Model.UserType;

/**
 * The main controller singleton used to manage the system
 * @author mbervet
 */
public class MainController {

    /**
     * The unique instance of the main controller
     */
    private static MainController Instance = new MainController();

    /**
     * A reference to the main dao to access to the data management
     */
    private MainDAO MainDAO;

    /**
     * The instance of the user controller
     */
    private UserController UserController;
    /**
     * The instance of the shop controller
     */
    private ShopController ShopController;
    /**
     * The instance of the shelf controller
     */
    private ShelfController ShelfController;
    /**
     * The instance of the category controller
     */
    private CategoryController CategoryController;
    /**
     * The instance of the product controller
     */
    private ProductController ProductController;
    /**
     * The instance of the item controller
     */
    private ItemController ItemController;

    /**
     * The constructor of the main controller
     * The access is restrained to block the number of instance and make a singleton
     * The constructor initialize all controllers
     */
    private MainController(){
        MainDAO = DAO.MainDAO.getInstance();

        UserController = new UserController(MainDAO);
        ShopController = new ShopController(MainDAO, UserController);
        ShelfController = new ShelfController(MainDAO, UserController);
        CategoryController = new CategoryController(MainDAO, UserController);
        ProductController = new ProductController(MainDAO,UserController);
        ItemController = new ItemController(MainDAO, UserController);

        InitializeAdminUser();
    }

    /**
     * This method initialize the user admin at the first launch of the API
     * This user is create only if there is no user in the system
     * At the first launch you have to change the password of the admin
     */
    private void InitializeAdminUser(){
        try {
            if (MainDAO.getUserDAO().getUsers().isEmpty()){
                MainDAO.getUserDAO().addUser("admin", "password", "", "", UserType.ADMIN);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method give the access to the unique instance of the main controller
     * @return MainController Instance: the reference to the unique instance
     */
    public static MainController getInstance(){
        return Instance;
    }

    /**
     * Give access to the user controller
     * @return UserController UserController : the reference to the user controller
     */
    public Controller.UserController getUserController() { return UserController; }

    /**
     * Give access to the shop controller
     * @return ShopController ShopController : the reference to the shop controller
     */
    public Controller.ShopController getShopController() { return ShopController; }

    /**
     * Give access to the shelf controller
     * @return ShelfController ShelfController : the reference to the shelf controller
     */
    public Controller.ShelfController getShelfController() { return ShelfController; }

    /**
     * Give access to the shelf controller
     * @return ShelfController ShelfController : the reference to the shelf controller
     */
    public Controller.CategoryController getCategoryController() { return CategoryController; }

    /**
     * Give access to the product controller
     * @return ProductController ProductController : the reference to the product controller
     */
    public Controller.ProductController getProductController() { return ProductController; }

    /**
     * Give access to the item controller
     * @return ItemController ItemController : the reference to the item controller
     */
    public Controller.ItemController getItemController() { return ItemController; }
}
