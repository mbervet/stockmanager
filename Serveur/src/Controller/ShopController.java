package Controller;

import DAO.MainDAO;
import Model.Shop;
import Model.User;
import Model.UserType;

import java.util.List;

/**
 * The shop controller
 * Managing access and dao for all shops
 * @author mbervet
 */
public class ShopController {

    /**
     * A reference to the main dao
     */
    private MainDAO MainDAO;

    /**
     * A reference to the user controller to check connection
     */
    private UserController UserController;

    /**
     * The constructor of shop controller
     * @param mainDAO : the reference to the main doa
     * @param userController : the reference to the user controller
     */
    public ShopController(MainDAO mainDAO, UserController userController){
        MainDAO = mainDAO;
        UserController = userController;
    }

    /**
     * Create a new shop and add it to the local instance and in the database
     * @param connectedEmail : the email of the connected user
     * @param address : the shop address
     * @param town : the shop town
     * @param managerEmail : the shop manager email
     * @return boolean added : true if the user is successfully added, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights or if the manager email doesn't match to an existing shop manager
     */
    public boolean addShop(String connectedEmail, String address, String town, String managerEmail) throws Exception {
        boolean added = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        User manager = MainDAO.getUserDAO().getUserByEmail(managerEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can add a new shop.");
        }
        if (manager.getType() != UserType.SHOP_MANAGER){
            throw new Exception("The given email doesn't correspond to a shop manager.");
        }

        if (MainDAO.getShopDAO().addShop(address, town, manager) != null){
            added = true;
        }

        return added;
    }

    /**
     * Return the list of all shops
     * @param connectedEmail : the email of the connected user
     * @return List<Shop> Shops : the list of shops
     * @throws Exception if the user is not connected
     */
    public List<Shop> getShopList(String connectedEmail) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getShopDAO().getShops();
    }

    /**
     * Return the information of the shop corresponding to the given id
     * @param connectedEmail : the email of the connected user
     * @param id : the shop is to look for
     * @return Shop shop : the corresponding shop or null
     * @throws Exception if the user is not connected
     */
    public Shop getShopInformationById(String connectedEmail, Integer id) throws Exception {
        UserController.isConnected(connectedEmail);

        return MainDAO.getShopDAO().getShopById(id);
    }

    /**
     * Change the manager of a shop
     * @param connectedEmail : the email of the connected user
     * @param shopId : the shop id to look for
     * @param managerEmail : the email of the new manager
     * @return boolean changed : true if the manager is successfully changed, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights or if the manager email doesn't match to an existing shop manager
     */
    public boolean changeManager(String connectedEmail, Integer shopId, String managerEmail) throws Exception {
        boolean changed = false;
        User connectedUser = UserController.isConnected(connectedEmail);
        Shop shop = MainDAO.getShopDAO().getShops().get(shopId);
        User manager = MainDAO.getUserDAO().getUserByEmail(managerEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can change the manager of the shop.");
        }
        if (manager.getType() != UserType.SHOP_MANAGER){
            throw new Exception("The given manager doesn't has the right to manage a shop.");
        }

        if (MainDAO.getShopDAO().updateShop(shop.getId(), shop.getAddress(), shop.getTown(), manager) != null){
            changed = true;
        }

        return changed;
    }

    /**
     * Modify shop information with the given data
     * @param connectedEmail : the email of the connected user
     * @param id : the shop id to look for
     * @param address : the new shop address
     * @param town : the new shop town
     * @return boolean modified : true if the information are successfully modified, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean modifyShopInformation(String connectedEmail, Integer id, String address, String town) throws Exception {
        boolean modified = false;
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can modify shops information.");
        }

        if (MainDAO.getShopDAO().updateShop(id, address, town, MainDAO.getShopDAO().getShopById(id).getManager()) != null){
            modified = true;
        }

        return modified;
    }

    /**
     * Remove the shop corresponding to the given id from the local instance and from the database
     * @param connectedEmail : the email of the connected user
     * @param id : the shop id to look for
     * @return boolean deleted : true if the shop is successfully deleted, false otherwise
     * @throws Exception if the user is not connected or if he doesn't have the rights
     */
    public boolean deleteShop(String connectedEmail, Integer id) throws Exception {
        User connectedUser = UserController.isConnected(connectedEmail);

        if (connectedUser.getType() != UserType.ADMIN && connectedUser.getType() != UserType.CEO){
            throw new Exception("Only the CEO can delete a shop.");
        }

        return MainDAO.getShopDAO().deleteShop(id);
    }
}
