package DAO;

import Model.Shelf;
import Model.Shop;
import Model.User;
import Utils.HibernateUtil;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The shelf data access object
 * Managing shelf data for local instance and in the database
 * @author mbervet
 */
public class ShelfDAO {

    /**
     * The list of shelf local instance
     */
    private List<Shelf> Shelfs;

    /**
     * A reference to the shop dao to manage shelf shop
     */
    private ShopDAO ShopDAO;

    /**
     * A reference to the user dao to manage shelf manager
     */
    private UserDAO UserDAO;

    /**
     * The constructor of the shelf dao
     * Initialize the list of shelf
     * Load data from database
     */
    public ShelfDAO(){
        Shelfs = new ArrayList<>();
        UserDAO = new UserDAO();
        ShopDAO = new ShopDAO();
        loadFromDB();

    }

    /**
     * The constructor of the shelf dao
     * Initialize the list of shelf
     * Load data from database
     */
    public ShelfDAO(ShopDAO shopDAO, UserDAO userDAO){
        Shelfs = new ArrayList<>();
        ShopDAO = shopDAO;
        UserDAO = userDAO;
        loadFromDB();
    }

    /**
     * Give access to the list of user
     * @return List<Shelf> Shelfs : the list of shelf instance
     */
    public List<Shelf> getShelfs() { return Shelfs; }

    /**
     * Load all shelf in database to fill the local instance
     */
    private void loadFromDB(){
        Session session = HibernateUtil.BeginTransaction();
        List<?> shelfs = session.createQuery("FROM Shelf").list();
        HibernateUtil.CommitTransaction(session);

        for (Object o : shelfs){
            Shelf shelf = (Shelf) o;
            Shelfs.add(shelf);
            ShopDAO.getShopById(shelf.getShop().getId()).getShelfs().add(shelf);
            UserDAO.getUserByEmail(shelf.getManager().getEmail()).getShelfs().add(shelf);
        }
    }

    /**
     * Look for a shelf with it's id
     * @param id : the shelf id to look for
     * @return Shelf shelf : the corresponding shelf or null
     */
    public Shelf getShelfById(int id){
        Shelf shelf = null;

        for (Iterator<Shelf> shelfIterator = Shelfs.iterator(); shelfIterator.hasNext() && shelf == null;){
            shelf = shelfIterator.next();
            if (!shelf.getId().equals(id)){
                shelf = null;
            }
        }

        return shelf;
    }

    /**
     * Create a new shelf with the given data and add it to the local instance and in the database
     * @param manager : the shelf manager
     * @param shop : the shelf shop
     * @return Integer id : the id of the new shelf
     * @throws Exception if the given shop is null
     */
    public Integer addShelf(User manager, Shop shop) throws Exception {
        Shelf shelf = new Shelf(manager, shop);

        if (shop == null){
            throw new Exception("The shelf shop has to be set.");
        }

        Session session = HibernateUtil.BeginTransaction();
        session.save(shelf);
        HibernateUtil.CommitTransaction(session);

        Shelfs.add(shelf);
        manager.getShelfs().add(shelf);
        shop.getShelfs().add(shelf);

        return shelf.getId();
    }

    /**
     * Update the shelf information with the given data
     * @param id : the id to look for
     * @param manager : the new manager
     * @param shop : the new shop
     * @return Integer id : the id of the modified shelf
     * @throws Exception if the given shop is null or if the given id doesn't match to an existing shelf
     */
    public Integer updateShelf(int id, User manager, Shop shop) throws Exception {
        Shelf shelf = getShelfById(id);

        if (shop == null){
            throw new Exception("The shelf shop has to be set.");
        }
        if (shelf == null){
            throw new Exception("This id doesn't refer to an existing shelf.");
        }

        shelf.setManager(manager);
        shelf.setShop(shop);

        Session session = HibernateUtil.BeginTransaction();
        session.saveOrUpdate(shelf);
        HibernateUtil.CommitTransaction(session);

        return shelf.getId();
    }

    /**
     * Remove the shelf corresponding to the given id from the local instance and the database
     * @param id : the id to look for
     * @return boolean deleted : true if the shelf is successfully deleted, false otherwise
     * @throws Exception if the given id doesn't match to an existing shelf or if the shelf still have items
     */
    public boolean deleteShelf(int id) throws Exception {
        Shelf shelf = getShelfById(id);

        if (shelf == null){
            throw new Exception("This id doesn't refer to an existing shelf.");
        }

        if (!shelf.getItems().isEmpty()){
            throw new Exception("This shelf still has items.");
        }

        Session session = HibernateUtil.BeginTransaction();
        session.createQuery("DELETE FROM Shelf WHERE Shelf.Id = :id").setParameter("id", shelf.getId());
        HibernateUtil.CommitTransaction(session);

        shelf.getShop().getShelfs().remove(shelf);
        shelf.getManager().getShelfs().remove(shelf);

        return Shelfs.remove(shelf);
    }
}
