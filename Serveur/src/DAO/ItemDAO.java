package DAO;

import Model.Item;
import Model.Product;
import Model.Shelf;
import Utils.HibernateUtil;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The item data access object
 * Managing item data for local instance and in the database
 * @author mbervet
 */
public class ItemDAO {

    /**
     * The list of item local instance
     */
    private List<Item> Items;
    private ProductDAO ProductDAO;
    private ShelfDAO ShelfDAO;

    /**
     * The constructor of the item dao
     * Initialize the list of item
     * Load data from database
     */
    public ItemDAO(ProductDAO productDAO, ShelfDAO shelfDAO){
        Items = new ArrayList<>();
        ProductDAO = productDAO;
        ShelfDAO = shelfDAO;
        loadFromDB();
    }

    /**
     * Give access to the list of item
     * @return List<Item> Items : the list of item instance
     */
    public List<Item> getItems(){ return Items; }

    /**
     * Load all items in database to fill the local instance
     */
    private void loadFromDB(){
        Session session = HibernateUtil.BeginTransaction();
        List<?> items = session.createQuery("FROM Item").list();
        HibernateUtil.CommitTransaction(session);

        for (Object o : items) {
            Item item = (Item) o;
            Items.add(item);
            ProductDAO.getProductByName(item.getProduct().getName()).getItems().add(item);
            ShelfDAO.getShelfById(item.getShelf().getId()).getItems().add(item);
        }
    }

    /**
     * Look for the item with it's code
     * @param code : the code to look for
     * @return Item item : the corresponding item or null
     */
    public Item getItemByCode(String code){
        Item item = null;

        for (Iterator<Item> itemIterator = Items.iterator(); itemIterator.hasNext() && item == null;){
            item = itemIterator.next();
            if (!item.getCode().equals(code)){
                item = null;
            }
        }

        return item;
    }

    /**
     * Create a new item with the given data and add it to the local instance and in the database
     * @param code : the item code
     * @param shelf : the item shelf
     * @param product : the item product
     * @return Integer id : the id of the new item
     * @throws Exception if the given product is null or if the given code is already taken by another item
     */
    public Integer addItem(String code, Shelf shelf, Product product) throws Exception {
        if (product == null){
            throw new Exception("The item product has to be set.");
        }

        if (getItemByCode(code) != null){
            throw new Exception("This item code already exist.");
        }

        Item item = new Item(code, shelf, product);

        Session session = HibernateUtil.BeginTransaction();
        session.save(item);
        HibernateUtil.CommitTransaction(session);

        Items.add(item);
        product.getItems().add(item);

        return item.getId();
    }

    /**
     * Update the item information with the given data
     * @param code : the code to look for
     * @param shelf : the new shelf
     * @param product : the new product
     * @return Integer id : the id of the modified item
     * @throws Exception if the given product is null or if the given code doesn't match to an existing item
     */
    public Integer updateItem(String code, Shelf shelf, Product product) throws Exception{
        Item item = getItemByCode(code);

        if (product == null){
            throw new Exception("The item product has to be set.");
        }

        if (item == null){
            throw new Exception("This code doesn't refer to an existing item.");
        }

        item.setShelf(shelf);
        item.setProduct(product);

        Session session = HibernateUtil.BeginTransaction();
        session.saveOrUpdate(item);
        HibernateUtil.CommitTransaction(session);

        return item.getId();
    }

    /**
     * Remove the item corresponding to the given code from the local instance and from the database
     * @param code : the code to look for
     * @return boolean deleted : true if the item is successfully deleted, false otherwise
     * @throws Exception if the given code doesn't match to an existing item
     */
    public boolean deleteItem(String code) throws Exception{
        Item item = getItemByCode(code);

        if (item == null){
            throw new Exception("This code doesn't refer to an existing item.");
        }

        Session session = HibernateUtil.BeginTransaction();
        session.createQuery("DELETE FROM Item WHERE Item.Id = :id").setParameter("id", item.getId());
        HibernateUtil.CommitTransaction(session);

        item.getProduct().getItems().remove(item);
        item.getShelf().getItems().remove(item);

        return Items.remove(item);
    }
}
