package DAO;

import Model.Shop;
import Model.User;
import Utils.HibernateUtil;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The shop data access object
 * Managing shop data for local instance and in the database
 * @author mbervet
 */
public class ShopDAO {

    /**
     * The list of shop local instance
     */
    private List<Shop> Shops;

    /**
     * A reference to the user dao to manage shop manager
     */
    private UserDAO UserDAO;


    /**
     * The constructor of the shop dao
     * Initialize the list of shops
     * Load data from database
     */
    public ShopDAO(){
        Shops = new ArrayList<>();
        UserDAO = new UserDAO();
        loadFromDB();
    }

    /**
     * The constructor of the shop dao
     * Initialize the list of shop
     * Load data from database
     */
    public ShopDAO(UserDAO userDAO){
        Shops = new ArrayList<>();
        UserDAO = userDAO;
        loadFromDB();
    }

    /**
     * Give access to the list of shop
     * @return List<Shop> Shops : the list of shop instance
     */
    public List<Shop> getShops() { return Shops; }

    /**
     * Load all shops in database to fill the local instance
     */
    private void loadFromDB(){
        Session session = HibernateUtil.BeginTransaction();
        List<?> shops = session.createQuery("FROM Shop").list();
        HibernateUtil.CommitTransaction(session);

        for (Object o : shops){
            Shop shop = (Shop) o;
            Shops.add(shop);
            UserDAO.getUserByEmail(shop.getManager().getEmail()).getShops().add(shop);
        }
    }

    /**
     * Look for a shop with it's id
     * @param id : the shop id
     * @return Shop shop : the shop corresponding to given id or null
     */
    public Shop getShopById(int id){
        Shop shop = null;

        for (Iterator<Shop> shopIterator = Shops.iterator(); shopIterator.hasNext() && shop == null;){
            shop = shopIterator.next();
            if (!shop.getId().equals(id)){
                shop = null;
            }
        }

        return shop;
    }

    /**
     * Create a new shop and add it to the local instance and in the database
     * @param address : the shop address
     * @param town : the shop town
     * @param manager : the shop manager
     * @return Integer id : the id of the shop
     */
    public Integer addShop(String address, String town, User manager) {
        Shop shop = new Shop(address,town, manager);

        Session session = HibernateUtil.BeginTransaction();
        session.save(shop);
        HibernateUtil.CommitTransaction(session);

        Shops.add(shop);
        manager.getShops().add(shop);

        return shop.getId();
    }

    /**
     * Update shop information with the given data
     * @param id : the current shop id
     * @param address : the new address
     * @param town : the new town
     * @param manager : the new manager
     * @return Integer id : the id of the modified shop
     * @throws Exception if the given id doesn't match to an existing shop
     */
    public Integer updateShop(int id, String address, String town, User manager) throws Exception {
        Shop shop = getShopById(id);

        if (shop == null){
            throw new Exception("This id doesn't refer to an existing shop.");
        }

        shop.setAddress(address);
        shop.setTown(town);
        shop.setManager(manager);

        Session session = HibernateUtil.BeginTransaction();
        session.saveOrUpdate(shop);
        HibernateUtil.CommitTransaction(session);

        return shop.getId();
    }

    /**
     * Remove the shop corresponding to the given id from the local instance ans the database
     * @param id : the shop id to look for
     * @return boolean deleted : true if the shop is successfully deleted
     * @throws Exception if the given id doesn't match to an existing shop or if the shop still have shelf
     */
    public boolean deleteShop(int id) throws Exception {
        Shop shop = getShopById(id);

        if (shop == null){
            throw new Exception("This id doesn't refer to an existing shop.");
        }

        if (!shop.getShelfs().isEmpty()){
            throw new Exception("This shop still has shelf.");
        }

        Session session = HibernateUtil.BeginTransaction();
        session.createQuery("DELETE FROM Shop WHERE Shop.Id = :id").setParameter("id", shop.getId());
        HibernateUtil.CommitTransaction(session);

        shop.getManager().getShops().remove(shop);

        return Shops.remove(shop);
    }
}
