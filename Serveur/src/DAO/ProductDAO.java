package DAO;

import Model.Category;
import Model.Product;
import Utils.HibernateUtil;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The product data access object
 * Managing product data for local instance and in the database
 * @author mbervet
 */
public class ProductDAO {

    /**
     * The list of product local instance
     */
    private List<Product> Products;

    private CategoryDAO CategoryDAO;

    public ProductDAO(){
        Products = new ArrayList<>();
        CategoryDAO = new CategoryDAO();
        loadFromDB();
    }

    /**
     * The constructor of the product dao
     * Initialize the list of product
     * Load data from database
     */
    public ProductDAO(CategoryDAO categoryDAO){
        Products = new ArrayList<>();
        CategoryDAO = categoryDAO;
        loadFromDB();
    }

    /**
     * Give access to the list of product
     * @return List<Product> Products : the list of product instance
     */
    public List<Product> getProducts() { return Products; }

    /**
     * Load all products in database to fill the local instance
     */
    private void loadFromDB(){
        Session session = HibernateUtil.BeginTransaction();
        List<?> products = session.createQuery("FROM Product").list();
        HibernateUtil.CommitTransaction(session);

        for (Object o : products) {
            Product product = (Product) o;
            Products.add(product);
            CategoryDAO.getCategoryByName(product.getCategory().getName()).getProducts().add(product);
        }
    }

    /**
     * Look for a product with it's name
     * @param name : the name to look for
     * @return Product product : the corresponding product or null
     */
    public Product getProductByName(String name){
        Product product = null;

        for (Iterator<Product> productIterator = Products.iterator(); productIterator.hasNext() && product == null;){
            product = productIterator.next();
            if (!product.getName().equals(name)){
                product = null;
            }
        }

        return product;
    }

    /**
     * Create a new product with the given data and add it to the local instance and in the database
     * @param name : the product name
     * @param description : the product description
     * @param price : the product price
     * @param category : the product category
     * @return Integer id : the id of the new product
     * @throws Exception if the given category is null or if the given name is already taken by a product
     */
    public Integer addProduct(String name, String description, double price, Category category) throws Exception{
        if (category == null){
            throw new Exception("The product category has to be set.");
        }

        if (getProductByName(name) != null){
            throw new Exception("This product name already exist.");
        }
        if(CategoryDAO.getCategoryByName(category.getName()) != null) {
            throw new Exception("This category does not exist.");

        }

        Product product = new Product(name, description, price, category);

        Session session = HibernateUtil.BeginTransaction();
        session.save(product);
        HibernateUtil.CommitTransaction(session);

        Products.add(product);
        category.getProducts().add(product);

        return product.getId();
    }

    /**
     * Update the product information with the given data
     * @param currentName : the current name to look for
     * @param newName : the new name
     * @param description : the new description
     * @param price : the new price
     * @param category : the new category
     * @return Integer id : the id of the modified product
     * @throws Exception if the given name doesn't match to an existing product or if the category is null or if the new name is already taken by another product
     */
    public Integer updateProduct(String currentName, String newName, String description, double price, Category category) throws Exception{
        Product product = getProductByName(currentName);

        if (product == null){
            throw new Exception("This name doesn't refer to an existing product.");
        }

        if (category == null){
            throw new Exception("The product category has to be set.");
        }

        if (!currentName.equals(newName) && getProductByName(newName) != null){
            throw new Exception("The new name refer to an existing product.");
        }
        if(CategoryDAO.getCategoryByName(category.getName()) == null) {
            throw new Exception("This category does not exist.");

        }

        product.setName(newName);
        product.setDescription(description);
        product.setPrice(price);
        product.setCategory(category);

        Session session = HibernateUtil.BeginTransaction();
        session.saveOrUpdate(product);
        HibernateUtil.CommitTransaction(session);

        return product.getId();
    }

    /**
     * Remove the product corresponding to given name from the local instance and form the database
     * @param name : the name to look for
     * @return boolean deleted : true if the product is successfully deleted, false otherwise
     * @throws Exception if the given name doesn't match to an existing product or if the product still have items
     */
    public boolean deleteProduct(String name) throws Exception {
        Product product = getProductByName(name);

        if (product == null){
            throw new Exception("This name doesn't refer to an existing product.");
        }

        if (!product.getItems().isEmpty()){
            throw new Exception("This product still has items.");
        }

        Session session = HibernateUtil.BeginTransaction();
        session.createQuery("DELETE FROM Product WHERE Product.Id = :id").setParameter("id", product.getId());
        HibernateUtil.CommitTransaction(session);

        product.getCategory().getProducts().remove(product);

        return Products.remove(product);
    }
}
