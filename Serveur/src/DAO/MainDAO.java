package DAO;

import Utils.HibernateUtil;

/**
 * The main data access object
 * Managing all data for local instance and in the database
 * @author mbervet
 */
public class MainDAO {

    /**
     * The necessary static instance of the main data access object to make a singleton
     */
    private static MainDAO Instance = new MainDAO();

    /**
     * The instance of the user data access object
     */
    private UserDAO UserDAO;
    /**
     * The instance of the shop data access object
     */
    private ShopDAO ShopDAO;
    /**
     * The instance of the shelf data access object
     */
    private ShelfDAO ShelfDAO;
    /**
     * The instance of the category data access object
     */
    private CategoryDAO CategoryDAO;
    /**
     * The instance of the product data access object
     */
    private ProductDAO ProductDAO;
    /**
     * The instance of the item data access object
     */
    private ItemDAO ItemDAO;

    /**
     * The constructor of the main dao
     * The access is restrained to block the number of instance and make a singleton
     * The constructor initialize all data access objects
     */
    private MainDAO(){
        UserDAO = new UserDAO();
        ShopDAO = new ShopDAO(UserDAO);
        ShelfDAO = new ShelfDAO(ShopDAO, UserDAO);
        CategoryDAO = new CategoryDAO();
        ProductDAO = new ProductDAO(CategoryDAO);
        ItemDAO = new ItemDAO(ProductDAO, ShelfDAO);
    }

    /**
     * The main dao destructor used to shutdown the hibernate session factory at the end of the programme
     * The method finalize is deprecated
     */
    @Deprecated
    protected void finalize(){
        HibernateUtil.shutdown();
    }

    /**
     * This method give the access to the unique instance of the main dao
     * @return MainDAO Instance: the reference to the unique instance
     */
    public static MainDAO getInstance(){
        return Instance;
    }

    /**
     * Give access to the user dao
     * @return UserDAO UserDAO : the reference to the user dao
     */
    public DAO.UserDAO getUserDAO() { return UserDAO; }

    /**
     * Give access to the shop dao
     * @return ShopDAO ShopDAO : the reference to the shop dao
     */
    public DAO.ShopDAO getShopDAO() { return ShopDAO; }

    /**
     * Give access to the shelf dao
     * @return ShelfDAO ShelfDAO : the reference to the shelf dao
     */
    public DAO.ShelfDAO getShelfDAO() { return ShelfDAO; }

    /**
     * Give access to the category dao
     * @return CategoryDAO CategoryDAO : the reference to the category dao
     */
    public DAO.CategoryDAO getCategoryDAO() { return CategoryDAO; }

    /**
     * Give access to the product dao
     * @return ProductDAO ProductDAO : the reference to the product dao
     */
    public DAO.ProductDAO getProductDAO() { return ProductDAO; }

    /**
     * Give access to the item dao
     * @return ItemDAO ItemDAO : the reference to the item dao
     */
    public DAO.ItemDAO getItemDAO() { return ItemDAO; }
}
