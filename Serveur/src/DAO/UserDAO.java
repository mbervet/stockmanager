package DAO;

import Model.User;
import Model.UserType;
import Utils.HibernateUtil;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The user data access object
 * Managing user data for local instance and in the database
 * @author mbervet
 */
public class UserDAO {

    /**
     * The list of user local instance
     */
    private List<User> Users;

    /**
     * The constructor of the user dao
     * Initialize the list of user
     * Load data from database
     */
    public UserDAO(){
        Users = new ArrayList<>();
        loadFromDB();
    }

    /**
     * Give access to the list of user
     * @return List<User> Users : the list of user instance
     */
    public List<User> getUsers() { return Users; }

    /**
     * Load all users in database to fill the local instance
     */
    private void loadFromDB(){
        Session session = HibernateUtil.BeginTransaction();
        List<?> users = session.createQuery("FROM User").list();
        HibernateUtil.CommitTransaction(session);

        for (Object o : users){
            User user = (User) o;
            Users.add(user);
        }
    }

    /**
     * Look for a user with his email
     * @param email : the user email you looking for
     * @return User user : the user corresponding to the given email, or null
     */
    public User getUserByEmail(String email){
        User user = null;

        for (Iterator<User> userIterator = Users.iterator(); userIterator.hasNext() && user == null;){
            user = userIterator.next();
            if (!user.getEmail().equals(email)){
                user = null;
            }
        }

        return user;
    }

    /**
     * Create a user and add it to the local instance and the database
     * @param email : the user email
     * @param password : the user password
     * @param firstName : the user first name
     * @param lastName : the user last name
     * @param type : the user type
     * @return Integer id : the id of the new user
     * @throws Exception if the email is already used
     */
    public Integer addUser(String email, String password, String firstName, String lastName, UserType type) throws Exception {
        if (getUserByEmail(email) != null){
            throw new Exception("This user email already exist.");
        }

        User user = new User(email, password, firstName, lastName, type);

        Session session = HibernateUtil.BeginTransaction();
        session.save(user);
        HibernateUtil.CommitTransaction(session);

        Users.add(user);

        return user.getId();
    }

    /**
     * Update user information with the given data
     * @param currentEmail : the current email of the user, used to find him
     * @param newEmail : the new email to set to the user
     * @param password : the new password
     * @param firstName : the new first name
     * @param lastName : the new last name
     * @param type : the new type
     * @return Integer id : the id of the modified user
     * @throws Exception if the current email doesn't match to an existing user or if the new email is already used by another user
     */
    public Integer updateUser(String currentEmail, String newEmail, String password, String firstName, String lastName, UserType type) throws Exception {
        User user = getUserByEmail(currentEmail);

        if (user == null){
            throw new Exception("This email doesn't refer to an exiting user.");
        }

        if (!currentEmail.equals(newEmail) && getUserByEmail(newEmail) != null){
            throw new Exception("This email doesn't refer to an existing user");
        }

        user.setEmail(newEmail);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setType(type);

        Session session = HibernateUtil.BeginTransaction();
        session.saveOrUpdate(user);
        HibernateUtil.CommitTransaction(session);

        return user.getId();
    }

    /**
     * Remove the user corresponding to the given email from the local instance and the database
     * @param email : the email to look for
     * @return boolean deleted : true if the user is successfully deleted, false otherwise
     * @throws Exception if the email doesn't match to an existing user or if the user is the admin
     */
    public boolean deleteUser(String email) throws Exception {
        User user = getUserByEmail(email);

        if (user == null){
            throw new Exception("This email doesn't refer to an exiting user.");
        }
        if (user.getType() == UserType.ADMIN){
            throw new Exception("The admin can't be deleted.");
        }

        Session session = HibernateUtil.BeginTransaction();
        session.delete(user);
        HibernateUtil.CommitTransaction(session);

        return Users.remove(user);
    }
}
