package DAO;

import Model.Category;
import Utils.HibernateUtil;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The category data access object
 * Managing category data for local instance and in the database
 * @author mbervet
 */
public class CategoryDAO {

    /**
     * The list of category local instance
     */
    private List<Category> Categories;

    /**
     * The constructor of the category dao
     * Initialize the list of category
     * Load data from database
     */
    public CategoryDAO(){
        Categories = new ArrayList<>();
        loadFromDB();
    }

    /**
     * Give access to the list of category
     * @return List<Category> Categories : the list of category instance
     */
    public List<Category> getCategories() { return Categories; }

    /**
     * Load all categories in database to fill the local instance
     */
    private void loadFromDB(){
        Session session = HibernateUtil.BeginTransaction();
        List<?> categories = session.createQuery("FROM Category").list();
        HibernateUtil.CommitTransaction(session);

        for (Object o : categories){
            Category category = (Category) o;
            Categories.add(category);
        }
    }

    /**
     * Look for a category with it's name
     * @param name : the category name to look for
     * @return Category category : the corresponding category or null
     */
    public Category getCategoryByName(String name){
        Category category = null;

        for (Iterator<Category> categoryIterator = Categories.iterator(); categoryIterator.hasNext() && category == null;){
            category = categoryIterator.next();
            if (!category.getName().equals(name)){
                category = null;
            }
        }

        return category;
    }

    /**
     * Create a new category and add it to the local instance and in the database
     * @param name : the category name
     * @param description : the category description
     * @return Integer id : the id of the new category
     * @throws Exception if the given name is already taken by another category
     */
    public Integer addCategory(String name, String description) throws Exception {
        if (getCategoryByName(name) != null){
            throw new Exception("This category name already exist.");
        }

        Category category = new Category(name, description);

        Session session = HibernateUtil.BeginTransaction();
        session.save(category);
        HibernateUtil.CommitTransaction(session);

        Categories.add(category);

        return category.getId();
    }

    /**
     * Update the category information with the given data
     * @param currentName : the name to look for
     * @param newName : the new name
     * @param description : the new description
     * @return Integer id : the id of the modified category
     * @throws Exception if the current name doesn't match to an existing category or if the new name is already taken by another category
     */
    public Integer updateCategory(String currentName, String newName, String description) throws Exception {
        Category category = getCategoryByName(currentName);

        if (category == null){
            throw new Exception("This name doesn't refer to an existing category.");
        }

        if (!currentName.equals(newName) && getCategoryByName(newName) != null){
            throw new Exception("This name refer to an existing category.");
        }

        category.setName(newName);
        category.setDescription(description);

        Session session = HibernateUtil.BeginTransaction();
        session.saveOrUpdate(category);
        HibernateUtil.CommitTransaction(session);

        return category.getId();
    }

    /**
     * Remove the category corresponding to the given name from the local instance and from the database
     * @param name : the name to look for
     * @return boolean deleted : true if the category is successfully deleted, false otherwise
     * @throws Exception if the given name doesn't match to an existing category of if the category still have products
     */
    public boolean deleteCategory(String name) throws Exception {
        Category category = getCategoryByName(name);

        if (category == null){
            throw new Exception("This name doesn't refer to an existing category.");
        }

        if (!category.getProducts().isEmpty()){
            throw new Exception("This category still has products.");
        }

        Session session = HibernateUtil.BeginTransaction();
        session.createQuery("DELETE FROM Category WHERE Category.Id = :id").setParameter("id", category.getId());
        HibernateUtil.CommitTransaction(session);

        return Categories.remove(category);
    }
}
