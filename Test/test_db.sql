-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 01 fév. 2020 à 14:52
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `stockmanager`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `Id` int(11) NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UK_eaqmeryfy1nk75chm93pu7gaa` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`Id`, `Description`, `Name`) VALUES
(1, 'ceci est une catégorie de test', 'category test 01'),
(2, 'desc update', 'category to update'),
(3, 'desc to delete', 'category to delete no product'),
(4, 'not empty category', 'category to delete with product');

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(7),
(7),
(7),
(7),
(7),
(7);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `Id` int(11) NOT NULL,
  `Code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProductId` int(11) DEFAULT NULL,
  `ShelfId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UK_avvvo3n945hud264hmoj6g8cm` (`Code`),
  KEY `FKerqg23l0l0n725aj8mg0cts8` (`ProductId`),
  KEY `FKjmgmfbfyisk5dsqpveb92ay19` (`ShelfId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `item`
--

INSERT INTO `item` (`Id`, `Code`, `ProductId`, `ShelfId`) VALUES
(1, 'item 01', 4, 5),
(2, 'item update', 2, 5),
(3, 'item delete', 4, 5);

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `Id` int(11) NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `CategoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UK_jenayn22g9j1yuvaarrr7pas3` (`Name`),
  KEY `FK6pnobu31k3yhhmk45s97imkui` (`CategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`Id`, `Description`, `Name`, `Price`, `CategoryId`) VALUES
(1, 'description product', 'product 01', 10, 4),
(2, 'desc to update', 'product to update', 0.1, 4),
(3, 'product to delete', 'product to delete', 0, 4),
(4, 'product not empty', 'product not empty', 0.101, 4);

-- --------------------------------------------------------

--
-- Structure de la table `shelf`
--

DROP TABLE IF EXISTS `shelf`;
CREATE TABLE IF NOT EXISTS `shelf` (
  `Id` int(11) NOT NULL,
  `ManagerId` int(11) DEFAULT NULL,
  `ShopId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKa0qyia9laak22gn5evi5wt6oe` (`ManagerId`),
  KEY `FKo6la05ofamqpf35r2benk8mvc` (`ShopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `shelf`
--

INSERT INTO `shelf` (`Id`, `ManagerId`, `ShopId`) VALUES
(1, 1, 4),
(2, 1, 4),
(3, 1, 4),
(4, 1, 4),
(5, 6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `shop`
--

DROP TABLE IF EXISTS `shop`;
CREATE TABLE IF NOT EXISTS `shop` (
  `Id` int(11) NOT NULL,
  `Address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ManagerId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK2uo1dtjblxj8sucjd1itv10jb` (`ManagerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `shop`
--

INSERT INTO `shop` (`Id`, `Address`, `Town`, `ManagerId`) VALUES
(1, 'test address', 'test town', 4),
(2, 'address to update', 'town to update', 6),
(3, 'address to delete', 'town to delete', 4),
(4, 'ADdRESS', 'town', 6);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(11) NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FirstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UK_7xyi34i31xehayqd3heubave8` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`Id`, `Email`, `FirstName`, `LastName`, `Password`, `Type`) VALUES
(1, 'test01@test.fr', 'Test 01', 'Test', 'test', 1),
(2, 'test02@test.fr', 'test 02', 'test', 'test', 2),
(3, 'test03@test.fr', 'test to update', 'test', 'test', 1),
(4, 'test04@test.fr', 'test 04', 'test', 'test', 3),
(5, 'test05@test.fr', 'test to delete', 'test', 'test', 0),
(6, 'test06@test.fr', 'Test admin', 'test', 'test', 4);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `FKerqg23l0l0n725aj8mg0cts8` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`),
  ADD CONSTRAINT `FKjmgmfbfyisk5dsqpveb92ay19` FOREIGN KEY (`ShelfId`) REFERENCES `shelf` (`Id`);

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK6pnobu31k3yhhmk45s97imkui` FOREIGN KEY (`CategoryId`) REFERENCES `category` (`Id`);

--
-- Contraintes pour la table `shelf`
--
ALTER TABLE `shelf`
  ADD CONSTRAINT `FKa0qyia9laak22gn5evi5wt6oe` FOREIGN KEY (`ManagerId`) REFERENCES `user` (`Id`),
  ADD CONSTRAINT `FKo6la05ofamqpf35r2benk8mvc` FOREIGN KEY (`ShopId`) REFERENCES `shop` (`Id`);

--
-- Contraintes pour la table `shop`
--
ALTER TABLE `shop`
  ADD CONSTRAINT `FK2uo1dtjblxj8sucjd1itv10jb` FOREIGN KEY (`ManagerId`) REFERENCES `user` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
