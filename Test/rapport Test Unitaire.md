# Mise en place des Test Unitaires:

1. Exécuter le script `test_db.db`sur la base de données de test.
2. Exécuter les test unitaires sur la base de données.


# Résultat des tests unitaires:
## CategoryDAOTest
### deleteCategoryEmptyProduct()
L'erreur vient d'un problème lié a l'initialisation des collections de `Product` par Hibernate.

## ItemDAOTest et ShelfDAOTest
L'ensemble des erreurs proviennent de l'initialisation des collections de `Product` par Hibernate


## ProductDAOTest
### updateProduct()
L'erreur provient d'un problème pour sauvegarder la catégorie du produit.
### deleteProduct()
L'erreur provient d'un problème pour récupérer les données des `Items` dans la base de données.
### addProduct()
L'erreur est dû au faite que la catégorie n'est pas présente dans la base de données.

## ShopDAOTest
### addShop()
L'erreur est dû à un problème pour sauvegarder le manager du magasin.
Ce problème peut être du au mock où l'utilisateur associé au magasin n'existe pas dans la base de données.
### updateShop()
L'erreur est dû à un problème pour sauvegarder le manager du magasin.
Ce problème peut être du au mock où l'utilisateur associé au magasin n'existe pas dans la base de données.
### deleteShop()
L'erreur est dû à un problème pour récupérer les données des `Items` dans la base de données.
